﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Exceptions
{
    public class OrchestratorApiException : Exception
    {
        public string StatusCode { get; set; }
        public Uri Uri { get; set; }
        public object Request { get; set; }
        public string Response { get; set; }
    }
}
