namespace Orchestrator.Domain.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
    }
}