﻿using Orchestrator.Data.Models;
using Orchestrator.Data.Repositories.Interfaces;
using Orchestrator.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Services
{
    public class ActionsDomain : IActionsDomain
    {
        private readonly IActionsRepository _actionsRepository;
        public ActionsDomain(IActionsRepository actionsRepository)
        {
            _actionsRepository = actionsRepository;
        }
        public IEnumerable<Acciones> GetActions()
        {

            return _actionsRepository.GetActions();
        }
    }
}
