﻿using DaVinci.Domain.Interfaces;
using Newtonsoft.Json;
using Orchestrator.Domain.Exceptions;
using Orchestrator.Domain.Models.ServiceModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Orchestrator.Domain.Http
{

    public class OrchestratorHttpClient : IOrchestratorHttpClient
    {
        private readonly HttpClient _httpClient;
        public OrchestratorHttpClient()
        {
            _httpClient = new HttpClient();
        }
        public async Task<ServiceResult> GetServiceResultAsync(Uri uri, CancellationToken cancellationToken)
        {
            using var request = new HttpRequestMessage(HttpMethod.Get, uri.AbsoluteUri);
            //request.Content = 
            var response = await _httpClient.SendAsync(request, cancellationToken);
            var content = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
                return new ServiceResult { ContentResult = content, IsValid = true, StatusCode = "200" };

            throw new OrchestratorApiException
            {
                StatusCode = response.StatusCode.ToString() ?? "Undefined status code",
                Uri = uri,
                Request = uri.Query,
                Response = content
            };
        }

        public async Task<T> GetASync<T>(Uri uri, CancellationToken cancellationToken)
        {
            using var request = new HttpRequestMessage(HttpMethod.Get, uri.AbsoluteUri);
            var response = await _httpClient.SendAsync(request, cancellationToken);
            var stream = await response.Content.ReadAsStreamAsync();

            if (response.IsSuccessStatusCode)
                return DeserializeJsonFromStream<T>(stream);

            var content = await StreamToStringAsync(stream);
            throw new OrchestratorApiException
            {
                StatusCode = response.StatusCode.ToString() ?? "Undefined status code",
                Uri = uri,
                Request = uri.Query,
                Response = content
            };
        }
        public async Task<IEnumerable<T>> GetListAsync<T>(Uri uri, CancellationToken cancellationToken)
        {

            using var request = new HttpRequestMessage(HttpMethod.Get, uri.AbsoluteUri);
            var response = await _httpClient.SendAsync(request, cancellationToken);
            var stream = await response.Content.ReadAsStreamAsync();

            if (response.IsSuccessStatusCode)
                return DeserializeJsonFromStream<IEnumerable<T>>(stream);

            var content = await StreamToStringAsync(stream);
            throw new OrchestratorApiException
            {
                StatusCode = response.StatusCode.ToString() ?? "Undefined status code",
                Uri = uri,
                Request = uri.Query,
                Response = content
            };
        }
        public virtual async Task<ServiceResult> PostAsync(Uri uri, object content, CancellationToken cancellationToken)
        {
            using var request = new HttpRequestMessage(HttpMethod.Post, uri.AbsoluteUri);
            using var httpContent = CreateHttpContent(content);
            request.Content = httpContent;

            using var response = await _httpClient
                .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken)
                .ConfigureAwait(false);
            var stringContent = await response.Content.ReadAsStringAsync();
            if (response.IsSuccessStatusCode)
            {
                return new ServiceResult { ContentResult = stringContent, IsValid = true, StatusCode = "200" };
            }
            else
            {               
               var JsonErrorResponse = JsonConvert.SerializeObject(response);            
               var objectErrorResponse = JsonConvert.DeserializeObject<ServiceErrorResult>(JsonErrorResponse);

               return new ServiceResult { ContentResult = JsonErrorResponse, IsValid = false, StatusCode = objectErrorResponse.StatusCode};
            }
        }
        private static T DeserializeJsonFromStream<T>(Stream stream)
        {
            if (stream == null || stream.CanRead == false)
                return default(T);

            using var sr = new StreamReader(stream);
            using var jtr = new JsonTextReader(sr);
            var js = new JsonSerializer();
            var searchResult = js.Deserialize<T>(jtr);
            return searchResult;
        }
        private static async Task<string> StreamToStringAsync(Stream stream)
        {
            string content = null;

            if (stream != null)
                using (var sr = new StreamReader(stream))
                    content = await sr.ReadToEndAsync();

            return content;
        }
        private void SerializeJsonIntoStream(object value, Stream stream)
        {
            using var sw = new StreamWriter(stream, new UTF8Encoding(false), 1024, true);
            using var jtw = new JsonTextWriter(sw) { Formatting = Formatting.None };
            var js = new JsonSerializer();
            js.Serialize(jtw, value);
            jtw.Flush();
        }
        private HttpContent CreateHttpContent(object content)
        {
            HttpContent httpContent = null;

            if (content != null)
            {
                if (content.GetType() == typeof(Dictionary<string, string>))
                {
                    httpContent = new FormUrlEncodedContent((Dictionary<string, string>)content);
                }
                else
                {
                    httpContent = new StringContent(content.ToString(), Encoding.UTF8, "application/json");
                }
            }
            return httpContent;
        }
    }
}
