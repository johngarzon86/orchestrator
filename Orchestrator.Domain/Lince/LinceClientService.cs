﻿using DaVinci.Domain.Interfaces;
using Newtonsoft.Json;
using Orchestrator.Domain.Interfaces;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.Lince.Search;
using Orchestrator.Domain.Models.ServiceModels;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Threading;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Implementations
{
    public class LinceClientService : ILinceClientService
    {
        private readonly IConfiguration _config;
        private readonly IOrchestratorHttpClient _daVinciHttpClient;

        public LinceClientService(IOrchestratorHttpClient daVinciHttpClient, IConfiguration config)
        {
            _config = config;
            _daVinciHttpClient = daVinciHttpClient;
        }
        private Uri CreateUriFromConfig(string serviceName)
        {
            var relativeUrl = _config.GetValue<string>(serviceName);
            var baseUrl = _config.GetValue<string>("LinceServices:baseUrl");
            return new Uri(new Uri(baseUrl), relativeUrl);
        }
        private Uri CreateSearchUriFromConfig(string serviceName)
        {
            var relativeUrl = _config.GetValue<string>(serviceName);

            var number = new Random().Next(0, 100);
            var probabilityProd = _config.GetValue<int>("General:probabilitySearchProd");
            string baseUrl;
            if (number <= probabilityProd)
            {
                baseUrl = _config.GetValue<string>("LinceServices:searchBaseUrlProd");
            }
            else
            {
                baseUrl = _config.GetValue<string>("LinceServices:searchBaseUrlCont");
            }
            return new Uri(new Uri(baseUrl), relativeUrl);
        }
        public async Task<ServiceResult> VerifyClientAsync(ClientData clientData)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entidad", _config.GetValue<string>("General:entityCode") },
                { "tipoIdentificacion", clientData.IdentificationType },
                { "numeroIdentificacion", clientData.IdentificationNumber },
                { "comercio", clientData.CommerceCode },
                { "nitAfiliado", clientData.MemberIdentificationNumber }
            };

            var veryfyClientUrl = CreateUriFromConfig("LinceServices:verifyClient");
            return await _daVinciHttpClient.PostAsync(veryfyClientUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> GetAccountsAsync(ClientData clientData)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "tipoIdGir", clientData.IdentificationType },
                { "idGir", clientData.IdentificationNumber }
            };

            var getAccountsUrl = CreateUriFromConfig("LinceServices:getAccounts");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> GetCommerceRates(GetCommerceSpecialRate commerceSpecialRate)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entidad", _config.GetValue<string>("General:entityCode") },
                { "comercio", commerceSpecialRate.Business},
                { "codLineaCredito", commerceSpecialRate.CodCreditLine}
            };
            var getAccountsUrl = CreateUriFromConfig("LinceServices:getCommerceRates");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> IsMemberAsync(MemberData memberData)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "idNumber", memberData.MemberIdentificationNumber }
            };

            var getAccountsUrl = CreateUriFromConfig("LinceServices:isMember");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> GetClientDataAsync(ClientData clientData)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entidad", _config.GetValue<string>("General:entityCode") },
                { "tipoId", clientData.IdentificationType },
                { "numeroId", clientData.IdentificationNumber }
            };

            var getAccountsUrl = CreateUriFromConfig("LinceServices:ivrData");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> GenerateTinAsync(ClientData clientData, TinData tinData)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "typeId", clientData.IdentificationType },
                { "idNumber", clientData.IdentificationNumber },
                { "date", tinData.Date },
                { "time", tinData.Hour },
                { "function", "1" }
            };
            var getAccountsUrl = CreateUriFromConfig("LinceServices:generateTin");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> ValidateTinAsync(ClientData clientData, TinData tinData)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "typeId", clientData.IdentificationType },
                { "idNumber", clientData.IdentificationNumber },
                { "date", tinData.Date },
                { "time", tinData.Hour },
                { "tin", tinData.Tin },
                { "function", "1" }
            };
            var getAccountsUrl = CreateUriFromConfig("LinceServices:validateTin");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> GetCommercebyMember(CommerceData model)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "tipoIdAfil", model.CommerceIdentificationType },
                { "idAfil", model.CommerceIdentificationNumber },
                { "extCodigo", model.CommerceCode },
                { "codPortafolio", model.Portfolio }
            };
            var veryfyClientUrl = CreateUriFromConfig("LinceServices:getLinceCommerceWithPortfolio");
            return await _daVinciHttpClient.PostAsync(veryfyClientUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> CheckDocumentSolicitudeAsync(DocumentData documentData, ClientData clientData)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entidad", _config.GetValue<string>("General:entityCode") },
                { "comercio", documentData.CommerceCode },
                { "estadoDescAutom", documentData.AutomaticDiscountStatus },
                { "fechaProceso", documentData.ProcessDate },
                { "tipoIdentificacion", clientData.IdentificationType},
                { "numeroIdentificacion", clientData.IdentificationNumber },
            };
            var getAccountsUrl = CreateUriFromConfig("LinceServices:pendingTransaction");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> AutomaticDiscountAsync(AutomaticDigitalDiscountActivation automaticDigitalDiscountActivation)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entidad", _config.GetValue<string>("General:entityCode")},
                { "claveDigitalWeb", automaticDigitalDiscountActivation.DigitalWebPassword },
                { "idAutorizacion", automaticDigitalDiscountActivation.IdAuthorization},
            };
            var initialRequestUrl = CreateUriFromConfig("LinceServices:automaticDiscount");
            return await _daVinciHttpClient.PostAsync(initialRequestUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> FinalInformationLinceApplicationRecord(InputConfirmationRequestBaseFinal inputRegistration)
        {
            var parameters = JsonConvert.SerializeObject(inputRegistration);
            var getAccountsUrl = CreateUriFromConfig("LinceServices:FinalApplicationRecord");
            return await _daVinciHttpClient.PostAsync(getAccountsUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> InitialRequestAsync(InitialRequestInputModel initialRequestInputModel)
        {
            var parameters = JsonConvert.SerializeObject(initialRequestInputModel);
            var initialRequestUrl = CreateUriFromConfig("LinceServices:initialRequest");
            return await _daVinciHttpClient.PostAsync(initialRequestUrl, parameters, CancellationToken.None);
        }
        private CancellationToken GetCustomCancellationToken()
        {
            var timeoutSeconds = _config.GetValue<int>("General:searchTimeoutSeconds");
            var cancellationToken = new CancellationTokenSource(TimeSpan.FromSeconds(timeoutSeconds));
            return cancellationToken.Token;
        }
        public async Task<ServiceResult> LinceBasicSearchHomeAsync(LinceBasicSearchFilter linceBasicSearchFilter)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "idAfil", linceBasicSearchFilter.idAfil },
                { "tipoIdAfil",linceBasicSearchFilter.tipoIdAfil},
                { "returnPlanPagos", linceBasicSearchFilter.returnPlanPagos},
                { "returnBeneficiarios", linceBasicSearchFilter.returnBeneficiarios},
                { "numReg", _config.GetValue<string>("General:NumberRecords")}

            };
            var initialRequestUrl = CreateSearchUriFromConfig("LinceServices:basicSearchHome");

            return await _daVinciHttpClient.PostAsync(initialRequestUrl, parameters, GetCustomCancellationToken());
        }

        public async Task<ServiceResult> LinceBasicSearchAsync(LinceBasicSearchFilter linceBasicSearchFilter)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "codAut", linceBasicSearchFilter.codAut },
                { "tipoIdGir", linceBasicSearchFilter.tipoIdGir },
                { "idGir", linceBasicSearchFilter.idGir },
                { "idAfil", linceBasicSearchFilter.idAfil },
                { "returnPlanPagos", linceBasicSearchFilter.returnPlanPagos},
                { "tipoIdAfil",linceBasicSearchFilter.tipoIdAfil},
                { "fechaAval",linceBasicSearchFilter.DateAval},
                { "returnBeneficiarios", linceBasicSearchFilter.returnBeneficiarios},
                { "numReg", _config.GetValue<string>("General:NumberRecords")},
            };
            var initialRequestUrl = CreateSearchUriFromConfig("LinceServices:basicSearch");
            return await _daVinciHttpClient.PostAsync(initialRequestUrl, parameters, GetCustomCancellationToken());
        }
        public async Task<ServiceResult> RecordDenialRequestAsync(RecordDenialRequestInputModel recordDenialRequestInputModel)
        {
            var parameters = JsonConvert.SerializeObject(recordDenialRequestInputModel);
            var initialRequestUrl = CreateUriFromConfig("LinceServices:denyRequest");
            return await _daVinciHttpClient.PostAsync(initialRequestUrl, parameters, CancellationToken.None);
        }
        public async Task<ServiceResult> LinceExtendedSearchAsync(LinceExtendedSearchFilter linceExtendedSearchFilter)
        {
            //var parameters = JsonConvert.SerializeObject(linceExtendedSearchFilter);
            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "tipoIdAfil", linceExtendedSearchFilter.tipoIdAfil},
                { "idAfil", linceExtendedSearchFilter.idAfil },
                { "numReg", _config.GetValue<string>("General:NumberRecords") },
                { "indTotalCons", linceExtendedSearchFilter.indTotalCons }

            };

            if (linceExtendedSearchFilter.codAut != null)
                parameters.Add("codAut", linceExtendedSearchFilter.codAut);
            if (linceExtendedSearchFilter.InitialDate != null)
                parameters.Add("fechaInicio", linceExtendedSearchFilter.InitialDate.Replace("/", ""));
            if (linceExtendedSearchFilter.FinalDate != null)
                parameters.Add("fechaFin", linceExtendedSearchFilter.FinalDate.Replace("/", ""));
            if (linceExtendedSearchFilter.idGir != null)
                parameters.Add("idGir", linceExtendedSearchFilter.idGir);
            if (linceExtendedSearchFilter.ReturnPlanPayment != null)
                parameters.Add("returnPlanPagos", linceExtendedSearchFilter.ReturnPlanPayment);
            if (linceExtendedSearchFilter.ReturnBeneficiaries != null)
                parameters.Add("returnBeneficiarios", linceExtendedSearchFilter.ReturnBeneficiaries);
            if (linceExtendedSearchFilter.tipoIdGir != null)
                parameters.Add("tipoIdGir", linceExtendedSearchFilter.tipoIdGir);

            var initialRequestUrl = CreateSearchUriFromConfig("LinceServices:extendedSearch");
            return await _daVinciHttpClient.PostAsync(initialRequestUrl, parameters, GetCustomCancellationToken());
        }

        public async Task<ServiceResult> SendLinceReport(LinceReportRequest linceReportRequest)
        {
            var email = linceReportRequest.Email1;
            if (!string.IsNullOrEmpty(linceReportRequest.Email2))
                email += $";{linceReportRequest.Email2}";
            if (!string.IsNullOrEmpty(linceReportRequest.Email3))
                email += $";{linceReportRequest.Email3}";

            var parameters = new Dictionary<string, string>
            {
                { "entity", _config.GetValue<string>("General:entityCode") },
                { "email",  email},
                { "fechaInicio", linceReportRequest.StartDate },
                { "fechaFin", linceReportRequest.EndDate },
                { "idAfil", linceReportRequest.MemberDocument },
                { "tipoIdAfil",linceReportRequest.MemberDocumentType}
            };
            var initialRequestUrl = CreateUriFromConfig("LinceServices:masiveReport");
            return await _daVinciHttpClient.PostAsync(initialRequestUrl, parameters, CancellationToken.None);
        }
    }
}
