﻿using Orchestrator.Data.Interfaces;
using Orchestrator.Data.Models;
using Orchestrator.Domain.Interfaces;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.ServiceModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Implementations
{
    public class LinceService : ILinceService
    {
        private readonly ILinceRepository _linceRepository;
        private readonly ILinceLocalDataRepository _LinceLocalDataRepository;

        public LinceService(ILinceRepository linceRepository, ILinceLocalDataRepository linceLocalDataRepository)
        {
            _linceRepository = linceRepository;
            _LinceLocalDataRepository = linceLocalDataRepository;
        }

        public IEnumerable<ViewTipoidentificacion> GetLinceDocumentTypes()
        {
            var result = _linceRepository.GetDocumetTypes();
            return result;
        }
        public IEnumerable<ViewAreageografica> GetLinceCities()
        {
            var result = _linceRepository.GetCities();
            return result;
        }
        public IEnumerable<ViewPais> GetLinceCountries()
        {
            var result = _linceRepository.GetCountries();
            return result;
        }
        public IEnumerable<ViewProfesion> GetLinceProfessions()
        {
            return _linceRepository.GetProfessions();
        }

        public IEnumerable<ViewOcupacion> GetOccupation()
        {
            return _linceRepository.GetOccupation();
        }

        public IEnumerable<ViewTiporelacionperson> GetTypeRelationPerson()
        {
            return _linceRepository.GetTypeRelationPerson();
        }

        public IEnumerable<ViewNiveldeestudio> GetLevelStudy()
        {
            return _linceRepository.GetLevelStudy();
        }

        public IEnumerable<ViewInstitucioneducativa> EducationalInstitution()
        {
            return _linceRepository.EducationalInstitution();
        }

        public IEnumerable<ApprovedProduct> GetLinceApprovedProducts(IEnumerable<CreditLine> lCreditLines, bool isLegalPerson, bool? isEducative = null)
        {
            return _linceRepository.GetApprovedProductLince(lCreditLines, isEducative.Value);
        }
        public ViewAreageografica GetLinceCitiesByCode(string cityCode)
        {
            return _linceRepository.GetCityByCode(cityCode);
        }
        public ProductoCanales GetApprovedProductById(int approvedProductId)
        {
            return _linceRepository.GetApprovedProductById(approvedProductId);
        }
        public ProductoCanalesHomologacionLince GetApprovedProduct(int approvedProductId)
        {
            return _linceRepository.GetApprovedProduct(approvedProductId);
        }
        public ViewTipoidentificacion GetLinceDocumentTypeByCode(string docType)
        {
            return _linceRepository.GetDocumetTypeByCode(docType);
        }
        public HomologacionPortafolio GetApprovedPortfolio(string approvedPortfolio)
        {
            return _linceRepository.GetApprovedPortfolio(approvedPortfolio);
        }
        public ServiceResult ValidateClientDocument(string documentType, string documentNumber)
        {
            ServiceResult sr = new ServiceResult();

            if (documentType == null || documentNumber == null) { throw new ArgumentNullException(); }

            if (!decimal.TryParse(documentNumber, out decimal docNumber))
            {
                sr.IsValid = false; sr.ErrorMessage = "El documento no es numérico";
                return sr;
            }

            var obj = _linceRepository.GetDocumetTypesByType(documentType);

            if (obj == null || string.IsNullOrEmpty(obj.CodigoTipoIdentificacion))
            {
                sr.ErrorMessage = "El tipo de documento seleccionado no es válido";
            }
            else if (obj.IndicadorValidacionRango.Equals("1"))
            {
                sr.ErrorMessage = docNumber.ToString().Trim().Length < obj.RangoInferiorIdentificacion ? "El número de identificación no es válido (rango min)" : docNumber.ToString().Trim().Length > obj.RangoSuperiorIdentificacion ? "El número de identificación no es válido (rango max)" : null;
            }
            if (obj.TipoPersonaAsociado.Equals("2") && obj.IndicadorValidaDígitodeChequeo.Equals("1"))
            {
                sr.ErrorMessage = !ValidateNit(documentNumber) ? "El dígito de verificación no es válido" : null;
            }
            sr.IsValid = sr.ErrorMessage == null ? true : false;
            return sr;
        }

        public ServiceResult ValidateServiceResponseAsync(dynamic resObj)
        {
            var messageList = new List<string>() { "Estimado afiliado, el dato ingresado para el comercio no es válido, por favor intente de nuevo con un código de afiliado válido.",
                "Estimado afiliado, el dato ingresado para el comercio no se encuentra en el sistema, por favor intente de nuevo con un código de afiliado válido." };
            var sr = new ServiceResult() { Error = new ServiceError() };
            sr.Error.ErrorMessage = resObj == null ? "SERVICE_EXCEPTION" : resObj.Status == 0 ? messageList[1]/*"LINCE_CODE_NOT_FOUND"*/ : (resObj.Status == 2 || resObj.Status == 3) ? messageList[0]/*"INVALID_LINCE_CODE"*/ : null;
            sr.IsValid = sr.Error.ErrorMessage == null ? true : false;
            return sr;
        }

        public bool ValidateTinGenerations(Dictionary<string, object> parameters)
        {
            var attemps = _linceRepository.GetTinGenerationAttemps(parameters);
            var maxGenerationAttemps = _linceRepository.MaxTinGenerationAttemps();
            if (attemps.Count == 0)
                return true;
            var unlockedDate = attemps.First().FechaRegistro.AddMinutes((int)parameters["unlockedTime"]);
            return attemps.Count() <= maxGenerationAttemps || DateTime.Now > unlockedDate;
        }
        public int GetGenerationUnlockedTime()
        {
            return _linceRepository.GenerationUnlockedTime();
        }
        public ServiceResult ValidateTinAttemps(Dictionary<string, object> parameters)
        {
            var sr = new ServiceResult() { IsValid = true };
            var attemps = _linceRepository.GetTinAttemps(parameters);
            var maxattemps = _linceRepository.MaxTinAttemps();
            if (!(attemps <= maxattemps))
            {
                sr.ErrorMessage = /*msj por parametros*/"Ha excedido el número de intentos posibles, por favor comuníquese con nosotros al 7440555 opción 1 en Bogotá o a nuestra línea nacional 01 8000 111 280 para tramitar su solicitud telefónica";
                sr.IsValid = false;
            }
            return sr;
        }
        public ServiceResult ValidateTinAttempsResponseAsync(Dictionary<string, object> parameters)
        {
            var result = (ServiceResult)parameters["resultTinValidation"];

            if (string.IsNullOrEmpty(result.ContentResult))
            { result.ErrorMessage = "No fue posible realizar la petición."; }
            else if (result.ContentResult == "0")
            { _ = AddLogTinAttemps(parameters); result.ErrorMessage = "Código de Seguridad errado, por favor intente de nuevo."; }

            result.IsValid = result.ErrorMessage == null ? true : false;
            return result;
        }
        public async Task AddLogTinGeneration(Dictionary<string, object> parameters)
        {
            await _linceRepository.SaveTinGenerationLog(parameters);
        }
        public async Task AddLogTinAttemps(Dictionary<string, object> parameters)
        {
            await _linceRepository.SaveTinAttempsLog(parameters);
        }
        public static bool ValidateNit(string nit)
        {
            long tempValue;
            if (nit.Length != 10)
                return false;
            if (!Int64.TryParse(nit, out tempValue))
                return false;

            var lenght = nit.Length;
            var verificationDigit = nit.Substring(lenght - 1, 1);
            int verificationDig;
            int.TryParse(verificationDigit, out verificationDig);
            var nitToValidate = nit.Substring(0, lenght - 1);
            int[] nums = { 3, 7, 13, 17, 19, 23, 29, 37, 41 };

            var sum = 0;
            for (int i = nitToValidate.Length - 1, j = 0; i >= 0; i--, j++)
            {
                var digit = -1;
                if (Char.IsDigit(nitToValidate[i]))
                    digit = Int32.Parse(nitToValidate[i].ToString());

                sum += digit * nums[j];
            }

            var dv = ((sum % 11) > 1 ? (11 - (sum % 11)) : (sum % 11));
            return dv == verificationDig;
        }
        public IEnumerable<ViewPais> GetCountries()
        {
            return _LinceLocalDataRepository.GetCountries();
        }

        public IEnumerable<ViewNiveldeestudio> GetEducationLevels()
        {
            return _LinceLocalDataRepository.GetEducationLevels();
        }
    }
}
