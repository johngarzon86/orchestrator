﻿using Orchestrator.Domain.Models.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DaVinci.Domain.Interfaces
{
    public interface IOrchestratorHttpClient
    {
        Task<T> GetASync<T>(Uri Uri, CancellationToken cancellationToken);
        Task<IEnumerable<T>> GetListAsync<T>(Uri uri, CancellationToken cancellationToken);
        Task<ServiceResult> GetServiceResultAsync(Uri uri, CancellationToken cancellationToken);
        Task<ServiceResult> PostAsync(Uri uri, object content, CancellationToken cancellationToken);

    }
}
