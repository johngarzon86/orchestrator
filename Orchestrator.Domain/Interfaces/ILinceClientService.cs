﻿//using Orchestrator.Domain.Models.Lince;
//using Orchestrator.Domain.Models.Lince.Search;
//using Orchestrator.Domain.Models.ServiceModels;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.Lince.Search;
using Orchestrator.Domain.Models.ServiceModels;
using Orchestrator.Domain.Models.ServiceModels;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Interfaces
{
    public interface ILinceClientService
    {
        Task<ServiceResult> VerifyClientAsync(ClientData clientData);
        Task<ServiceResult> GetAccountsAsync(ClientData clientData);
        Task<ServiceResult> IsMemberAsync(MemberData memberData);
        Task<ServiceResult> GetClientDataAsync(ClientData clientData);
        Task<ServiceResult> GenerateTinAsync(ClientData clientData, TinData tinData);
        Task<ServiceResult> ValidateTinAsync(ClientData clientData, TinData tinData);
        Task<ServiceResult> GetCommercebyMember(CommerceData model);
        Task<ServiceResult> CheckDocumentSolicitudeAsync(DocumentData documentData, ClientData clientData);
        Task<ServiceResult> AutomaticDiscountAsync(AutomaticDigitalDiscountActivation automaticDigitalDiscountActivation);
        Task<ServiceResult> InitialRequestAsync(InitialRequestInputModel initialRequestInputModel);
        Task<ServiceResult> FinalInformationLinceApplicationRecord(InputConfirmationRequestBaseFinal inputRegistration);
        Task<ServiceResult> LinceBasicSearchAsync(LinceBasicSearchFilter linceBasicSearchFilter);
        Task<ServiceResult> LinceBasicSearchHomeAsync(LinceBasicSearchFilter linceBasicSearchFilter);
        Task<ServiceResult> RecordDenialRequestAsync(RecordDenialRequestInputModel recordDenialRequestInputModel);
        Task<ServiceResult> LinceExtendedSearchAsync(LinceExtendedSearchFilter linceExtendedSearchFilter);
        Task<ServiceResult> GetCommerceRates(GetCommerceSpecialRate commerceSpecialRate);
        Task<ServiceResult> SendLinceReport(LinceReportRequest linceReportRequest);
    }
}
