﻿using Orchestrator.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Interfaces
{
    public interface IActionsDomain
    {
        IEnumerable<Acciones> GetActions();
    }
}
