﻿using Orchestrator.Data.Models;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Interfaces
{
    public interface ILinceService
    {
        IEnumerable<ViewTipoidentificacion> GetLinceDocumentTypes();
        IEnumerable<ViewAreageografica> GetLinceCities();
        IEnumerable<ViewPais> GetLinceCountries();
        IEnumerable<ViewProfesion> GetLinceProfessions();
        ServiceResult ValidateClientDocument(string documentType, string documentNumber);
        ServiceResult ValidateServiceResponseAsync(dynamic resObj);
        IEnumerable<ApprovedProduct> GetLinceApprovedProducts(IEnumerable<CreditLine> lCreditLines, bool isLegalPerson, bool? isEducative = null);
        ViewAreageografica GetLinceCitiesByCode(string cityCode);
        ProductoCanales GetApprovedProductById(int approvedProductId);
        ProductoCanalesHomologacionLince GetApprovedProduct(int approvedProductId);
        HomologacionPortafolio GetApprovedPortfolio(string approvedPortfolio);
        ViewTipoidentificacion GetLinceDocumentTypeByCode(string docType);
        bool ValidateTinGenerations(Dictionary<string, object> parameters);
        int GetGenerationUnlockedTime();
        Task AddLogTinGeneration(Dictionary<string, object> parameters);
        Task AddLogTinAttemps(Dictionary<string, object> parameters);
        ServiceResult ValidateTinAttemps(Dictionary<string, object> parameters);
        ServiceResult ValidateTinAttempsResponseAsync(Dictionary<string, object> parameters);
        //List<ListViewModel> GetHoldingService(string CommerceCategory);
        //IEnumerable<DigitalTransactionViewModel> ValidateDigitalTransactionType(Dictionary<string, object> parameters);

        IEnumerable<ViewPais> GetCountries();

        IEnumerable<ViewNiveldeestudio> GetEducationLevels();

        IEnumerable<ViewOcupacion> GetOccupation();

        IEnumerable<ViewTiporelacionperson> GetTypeRelationPerson();

        IEnumerable<ViewNiveldeestudio> GetLevelStudy();

        IEnumerable<ViewInstitucioneducativa> EducationalInstitution();
    }
}
