﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Table("SolicitudAval", Schema = "aval")]
    public partial class SolicitudAval
    {
        public SolicitudAval()
        {
            Beneficiarios = new HashSet<Beneficiarios>();
            InformacionNegocio = new HashSet<InformacionNegocio>();
            InformacionPreliminar = new HashSet<InformacionPreliminar>();
        }

        [Key]
        public int Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaCreacion { get; set; }
        [Required]
        [StringLength(50)]
        public string UsuarioCreacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaUltimaModificacion { get; set; }
        [StringLength(50)]
        public string UsuarioUltimaModificacion { get; set; }
        public bool Activo { get; set; }
        [Required]
        [StringLength(50)]
        public string DocumentoGirador { get; set; }
        [Required]
        [StringLength(2)]
        public string TipoDocumentoGirador { get; set; }
        [Required]
        [StringLength(50)]
        public string DocumentoAfiliado { get; set; }
        [Required]
        [StringLength(3)]
        public string TipoDocumentoAfiliado { get; set; }
        public int SectorId { get; set; }
        public bool Juridica { get; set; }
        [Required]
        [StringLength(20)]
        public string CodigoAfiliado { get; set; }
        [StringLength(2)]
        public string Portafolio { get; set; }
        public int? NumeroSolicitudLince { get; set; }
        [StringLength(2)]
        public string CausalNegacion { get; set; }
        [StringLength(40)]
        public string DescripcionNegacion { get; set; }
        /// <summary>
        /// llave foranea tabla Procesos
        /// </summary>
        public int? IdProceso { get; set; }
        /// <summary>
        /// Respuesta del servicio tercero
        /// </summary>
        public byte? RespuestaTercero { get; set; }

        [ForeignKey(nameof(SectorId))]
        [InverseProperty("SolicitudAval")]
        public virtual Sector Sector { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<Beneficiarios> Beneficiarios { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<InformacionNegocio> InformacionNegocio { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<InformacionPreliminar> InformacionPreliminar { get; set; }
    }
}