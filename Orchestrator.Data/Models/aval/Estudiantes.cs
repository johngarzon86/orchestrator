﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Table("Estudiantes", Schema = "aval")]
    public partial class Estudiantes
    {
        [Key]
        [Column("compania")]
        public int Compania { get; set; }
        [Key]
        public int SolicitudId { get; set; }
        public bool EstudianteMismoGiradorSolicitud { get; set; }
        [Key]
        [StringLength(6)]
        public string TipoDeIdentificacionEstudiante { get; set; }
        [Key]
        [Column(TypeName = "numeric(16, 0)")]
        public decimal NoIdentificacionEstudiante { get; set; }
        [Column("aval_Estudiantes")]
        public string AvalEstudiantes { get; set; }
        [Required]
        [StringLength(36)]
        public string PrimerNombreEstudiante { get; set; }
        [StringLength(36)]
        public string SegundoNombreEstudiante { get; set; }
        [Required]
        [StringLength(54)]
        public string PrimerApellidoEstudiante { get; set; }
        [StringLength(36)]
        public string SegundoApellidoEstudiante { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechadeNacimientoEstudiante { get; set; }
        [Required]
        [StringLength(9)]
        public string GeneroEstudiante { get; set; }
        [Required]
        [StringLength(6)]
        public string EstadocivilEstudiante { get; set; }
        [Required]
        [StringLength(250)]
        public string DireccionResidenciaEstudiante { get; set; }
        [StringLength(30)]
        public string CiudadDepartamentoEstudiante { get; set; }
        [Required]
        [StringLength(4)]
        public string TipodeTelefonoEstudiante { get; set; }
        [Required]
        [StringLength(8)]
        public string LocalidadEstudiante { get; set; }
        [Required]
        [StringLength(24)]
        public string BarrioEstudiante { get; set; }
        [Required]
        [StringLength(150)]
        public string CorreoElectronicoEstudiante { get; set; }
        public bool EstudianteTrabajaActualmenteEstudiante { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? SalarioEstudiante { get; set; }
        [StringLength(5)]
        public string RelacionConElDeudorEstudiante { get; set; }
        [Required]
        [StringLength(32)]
        public string CentroEducativoEstudiante { get; set; }
        [Required]
        [StringLength(8)]
        public string TipodeProgramaEstudiante { get; set; }
        [Required]
        [StringLength(22)]
        public string ProgramaEstudiante { get; set; }
        [Column("SemestreACursarEstudiante")]
        public int SemestreAcursarEstudiante { get; set; }
        [Column("fuente")]
        [StringLength(32)]
        public string Fuente { get; set; }
        [Column("fuenteImport")]
        [StringLength(32)]
        public string FuenteImport { get; set; }
        [Column("proceso")]
        public int? Proceso { get; set; }
        [Column("fecha_computador", TypeName = "datetime")]
        public DateTime? FechaComputador { get; set; }
        [Column("usuario")]
        [StringLength(32)]
        public string Usuario { get; set; }
        [Required]
        [Column("timestamp")]
        public byte[] Timestamp { get; set; }
        [Column("secuencia")]
        public int Secuencia { get; set; }
        [Required]
        [StringLength(20)]
        public string NumeroTelefono1Estudiante { get; set; }
        [Required]
        [StringLength(16)]
        public string Ciudad1Estudiante { get; set; }
        [StringLength(20)]
        public string NumeroTelefono2Estudiante { get; set; }
        [StringLength(16)]
        public string Ciudad2Estudiante { get; set; }
        [StringLength(20)]
        public string NumeroTelefono3Estudiante { get; set; }
        [StringLength(16)]
        public string Ciudad3Estudiante { get; set; }
        [Required]
        [StringLength(20)]
        public string NumeroCelularEstudiante { get; set; }
        public int IntentosDireccionResidenciaEstudiante { get; set; }

        [ForeignKey("Compania,SolicitudId")]
        [InverseProperty("Estudiantes")]
        public virtual Solicitudes Solicitudes { get; set; }
    }
}