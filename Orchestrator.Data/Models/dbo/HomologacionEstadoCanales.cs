﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    public partial class HomologacionEstadoCanales
    {
        [Key]
        public int IdHomologacion { get; set; }
        [Required]
        [StringLength(100)]
        public string EstadoOrquestador { get; set; }
        [Required]
        [StringLength(100)]
        public string EstadoCanales { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaRegistro { get; set; }
    }
}