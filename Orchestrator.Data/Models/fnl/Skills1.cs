﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Table("Skills", Schema = "fnl")]
    public partial class Skills1
    {
        [Key]
        public int SkillId { get; set; }
        [Required]
        [StringLength(64)]
        public string Descripcion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaRegistro { get; set; }
        [Required]
        public bool? Activo { get; set; }
    }
}