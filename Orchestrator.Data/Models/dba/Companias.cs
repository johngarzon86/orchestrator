﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Keyless]
    [Table("companias", Schema = "dba")]
    public partial class Companias
    {
        [Column("codigo", TypeName = "numeric(5, 0)")]
        public decimal Codigo { get; set; }
        [Required]
        [Column("razon_social")]
        [StringLength(100)]
        public string RazonSocial { get; set; }
        [Required]
        [Column("direccion")]
        [StringLength(100)]
        public string Direccion { get; set; }
        [Required]
        [Column("ruc")]
        [StringLength(15)]
        public string Ruc { get; set; }
        [Required]
        [Column("ciudad")]
        [StringLength(50)]
        public string Ciudad { get; set; }
        [Required]
        [Column("fax")]
        [StringLength(50)]
        public string Fax { get; set; }
        [Required]
        [Column("telefonos")]
        [StringLength(50)]
        public string Telefonos { get; set; }
        [Required]
        [Column("representante_legal")]
        [StringLength(50)]
        public string RepresentanteLegal { get; set; }
        [Required]
        [Column("contador")]
        [StringLength(50)]
        public string Contador { get; set; }
        [Column("ruc_representante")]
        [StringLength(15)]
        public string RucRepresentante { get; set; }
        [Column("ruc_contador")]
        [StringLength(15)]
        public string RucContador { get; set; }
        [Column("matricula_contador")]
        [StringLength(15)]
        public string MatriculaContador { get; set; }
        [Column("registro_mercantil")]
        [StringLength(15)]
        public string RegistroMercantil { get; set; }
        [Column("año_contable")]
        [StringLength(50)]
        public string AñoContable { get; set; }
        [Column("usuario")]
        [StringLength(32)]
        public string Usuario { get; set; }
        [Column("fecha_computador", TypeName = "datetime")]
        public DateTime FechaComputador { get; set; }
        [Column("secuencia_ocp", TypeName = "numeric(8, 0)")]
        public decimal? SecuenciaOcp { get; set; }
        [Column("abreviatura")]
        [StringLength(10)]
        public string Abreviatura { get; set; }
    }
}