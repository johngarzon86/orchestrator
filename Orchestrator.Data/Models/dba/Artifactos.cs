﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Table("artifactos", Schema = "dba")]
    public partial class Artifactos
    {
        [Key]
        [Column("tipo")]
        [StringLength(5)]
        public string Tipo { get; set; }
        [Required]
        [Column("subtipo")]
        [StringLength(5)]
        public string Subtipo { get; set; }
        [Key]
        [Column("nombre")]
        [StringLength(64)]
        public string Nombre { get; set; }
        [Required]
        [Column("modulo")]
        [StringLength(32)]
        public string Modulo { get; set; }
        [Column("subModulo")]
        [StringLength(32)]
        public string SubModulo { get; set; }
        [Column("dato")]
        public byte[] Dato { get; set; }
        [Required]
        [Column("usuario")]
        [StringLength(32)]
        public string Usuario { get; set; }
        [Column("fecha_computador", TypeName = "datetime")]
        public DateTime FechaComputador { get; set; }
        [Column("comentario")]
        public string Comentario { get; set; }
        [Required]
        [Column("comprimido")]
        [StringLength(10)]
        public string Comprimido { get; set; }
        [Column("menuid")]
        public int Menuid { get; set; }
    }
}