﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Table("MenuHistorias", Schema = "ark")]
    public partial class MenuHistorias
    {
        [Key]
        [Column("compania")]
        public int Compania { get; set; }
        [Key]
        public int IdMenu { get; set; }
        [Key]
        [Column("idHistoria")]
        public int IdHistoria { get; set; }
        [Column("asignado")]
        [StringLength(50)]
        public string Asignado { get; set; }
        [Column("estimado", TypeName = "numeric(18, 2)")]
        public decimal Estimado { get; set; }
        [Column("consumido", TypeName = "numeric(18, 2)")]
        public decimal Consumido { get; set; }
        [Column("usado", TypeName = "numeric(18, 2)")]
        public decimal Usado { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Inicio { get; set; }
        [Column("finalizado", TypeName = "datetime")]
        public DateTime? Finalizado { get; set; }
        [Required]
        [Column("estado")]
        [StringLength(50)]
        public string Estado { get; set; }
        [Column("usuario")]
        [StringLength(32)]
        public string Usuario { get; set; }
        [Column("fecha_computador", TypeName = "datetime")]
        public DateTime? FechaComputador { get; set; }
    }
}