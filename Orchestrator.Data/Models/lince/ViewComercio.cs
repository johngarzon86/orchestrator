﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Keyless]
    public partial class ViewComercio
    {
        [Required]
        [StringLength(4)]
        public string CodigoEntidad { get; set; }
        [Required]
        [StringLength(10)]
        public string CodigoComercio { get; set; }
        [StringLength(40)]
        public string NombreComercio { get; set; }
        [Column(TypeName = "decimal(9, 0)")]
        public decimal? CodigoPersona { get; set; }
        [StringLength(8)]
        public string Multicomercio { get; set; }
        [StringLength(1)]
        public string TipodePago { get; set; }
        [StringLength(2)]
        public string EstadoComercio { get; set; }
        [StringLength(2)]
        public string CodigoTipoDireccionUbicacion { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? ConsecutivoDireccion { get; set; }
        [StringLength(4)]
        public string CodigoCadenaComercial { get; set; }
        [StringLength(1)]
        public string NivelGeneracionDeposito { get; set; }
        [StringLength(4)]
        public string CategoriaComercio { get; set; }
        [StringLength(4)]
        public string CodigoAgencia { get; set; }
        [Column(TypeName = "decimal(9, 0)")]
        public decimal? CodigoPersonaPromotor { get; set; }
        [StringLength(1)]
        public string IndicadorContribuyenteEspecial { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal? PorcentajeContribuyenteEspecial { get; set; }
        [Column("CodigoBaseIVA")]
        [StringLength(2)]
        public string CodigoBaseIva { get; set; }
        [StringLength(1)]
        public string IndicadorAfiliacionTemporal { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaFinAfiliacion { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? TiempoenelMercadoAnos { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? TiempoenelMercadoMeses { get; set; }
        [Column(TypeName = "decimal(9, 0)")]
        public decimal? RecomendadoPor { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? CantidadPuntosdeVenta { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? CantidadTroqueladoras { get; set; }
        [StringLength(1)]
        public string HorariodeTrabajo { get; set; }
        [StringLength(1)]
        public string ZonadeUbicacion { get; set; }
        [Column(TypeName = "decimal(7, 0)")]
        public decimal? TamanodelLocal { get; set; }
        [StringLength(1)]
        public string TipodeInmueble { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechadeAdquisicion { get; set; }
        [StringLength(1)]
        public string LocalCompartido { get; set; }
        [StringLength(1)]
        public string BienServicioCorrespondeconCategoria { get; set; }
        [StringLength(1)]
        public string InventarioMercanciaVisible { get; set; }
        [StringLength(1)]
        public string SocioPropietarioOtroComercio { get; set; }
        [StringLength(40)]
        public string NombreComercioSocio { get; set; }
        [StringLength(1)]
        public string AvisodeDenominacionVisible { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? NúmeroCajasRegistradoras { get; set; }
        [Column(TypeName = "decimal(7, 0)")]
        public decimal? NúmeroTransaccionMensualesconTarjeta { get; set; }
        [Column(TypeName = "decimal(9, 0)")]
        public decimal? FuncionarioRealizoInspeccion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechadelaInspeccion { get; set; }
        [StringLength(1)]
        public string IndicadorPropina { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? PlazoDeposito { get; set; }
        [StringLength(1)]
        public string IndicadorValidacionCodigodeSeguridad { get; set; }
        [StringLength(1)]
        public string IndicadorExclusionDepuracion { get; set; }
        [StringLength(2)]
        public string CodigoCanal { get; set; }
        [StringLength(4)]
        public string CodigoCampaña { get; set; }
        [StringLength(1)]
        public string IndicadorMulticomercio { get; set; }
        [Column(TypeName = "decimal(9, 0)")]
        public decimal? CodigoPersonaEjecutivo { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaAfiliacion { get; set; }
        [Column(TypeName = "decimal(9, 0)")]
        public decimal? PersonaContacto { get; set; }
        [StringLength(4)]
        public string CentroComercial { get; set; }
        [StringLength(4)]
        public string ReddeTiendas { get; set; }
        [Column(TypeName = "decimal(5, 2)")]
        public decimal? ImpuestoSobrelaRentaDefault { get; set; }
        [StringLength(2)]
        public string EsquemaDiasdeDiferidoDefault { get; set; }
        [StringLength(1)]
        public string CaptacionClientes { get; set; }
        [StringLength(1)]
        public string RecepcionPagos { get; set; }
        [StringLength(2)]
        public string CodigoLineaConsumo { get; set; }
        [Column(TypeName = "decimal(9, 0)")]
        public decimal? NumeroCuenta { get; set; }
        [Column(TypeName = "decimal(3, 0)")]
        public decimal? NumeroTitularAdicional { get; set; }
    }
}