﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Table("CSCACTEC", Schema = "lince")]
    public partial class Cscactec
    {
        [Key]
        [Column("AECCAECAK")]
        [StringLength(4)]
        public string Aeccaecak { get; set; }
        [Column("AECDAECAF")]
        [StringLength(40)]
        public string Aecdaecaf { get; set; }
        [Column("AECTPACVF")]
        [StringLength(1)]
        public string Aectpacvf { get; set; }
        [Column("X_UPID", TypeName = "decimal(7, 0)")]
        public decimal? XUpid { get; set; }
        [Column("X_RRNO", TypeName = "decimal(15, 0)")]
        public decimal? XRrno { get; set; }
    }
}