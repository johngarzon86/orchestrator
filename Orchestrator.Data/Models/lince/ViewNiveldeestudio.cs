﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Keyless]
    public partial class ViewNiveldeestudio
    {
        [Required]
        [StringLength(2)]
        public string CodigoNiveldeEstudio { get; set; }
        [StringLength(25)]
        public string DescripcionNiveldeEstudio { get; set; }
    }
}