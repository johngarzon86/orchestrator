﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Keyless]
    public partial class ViewAgencia
    {
        [Required]
        [StringLength(4)]
        public string CodigoEntidad { get; set; }
        [Required]
        [StringLength(4)]
        public string CodigoAgencia { get; set; }
        [StringLength(40)]
        public string NombreAgencia { get; set; }
    }
}