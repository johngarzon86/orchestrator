﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Orchestrator.Data.Models
{
    [Table("ControlSolicitudesCupo", Schema = "lince")]
    public partial class ControlSolicitudesCupo
    {
        [Key]
        [StringLength(16)]
        public string Nit { get; set; }
        [Key]
        [StringLength(16)]
        public string CedulaGirador { get; set; }
        [Key]
        [Column(TypeName = "date")]
        public DateTime FechaControl { get; set; }
        public bool? Servicio1 { get; set; }
        public bool? Servicio2 { get; set; }
        public bool? Servicio3 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaRegistro { get; set; }
        [StringLength(32)]
        public string UsuarioRegistro { get; set; }
        /// <summary>
        /// Indica si el servicio 4 fue consumido
        /// </summary>
        public bool? Servicio4 { get; set; }
    }
}