﻿using Orchestrator.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Orchestrator.Data.Models;
using System.Linq;
 
namespace Orchestrator.Data.Repositories.Lince
{
    public class LinceLocalDataRepository : ILinceLocalDataRepository
    {
        private readonly CanalesvirtualesContext _context;
        public LinceLocalDataRepository(CanalesvirtualesContext context)
        {
            _context = context;
        }
        public IEnumerable<ViewAgencia> GetAgencies()
        {
            return _context.ViewAgencia;
        }

        public IEnumerable<ViewCampanA> GetCampaigns()
        {
            return _context.ViewCampanA;
        }

        public IEnumerable<ViewCanal> GetChannels()
        {
            return _context.ViewCanal;
        }

        public IEnumerable<ViewComercio> GetCommerces()
        {
            return _context.ViewComercio;
        }

        public IEnumerable<ViewTipoempresa> GetCompanyTypes()
        {
            return _context.ViewTipoempresa;
        }

        public IEnumerable<ViewPais> GetCountries()
        {
            return (from p in _context.ViewPais select p).ToList();                      
        }

        public IEnumerable<ViewAreageografica> GetDemographicAreas()
        {
            return _context.ViewAreageografica;
        }

        public IEnumerable<ViewActividadeconomica> GetEconomicActivities()
        {
            return _context.ViewActividadeconomica;
        }

        public IEnumerable<ViewInstitucioneducativa> GetEducationalInstitutions()
        {
            return _context.ViewInstitucioneducativa;
        }

        public IEnumerable<ViewNiveldeestudio> GetEducationLevels()
        {            
            return (from n in _context.ViewNiveldeestudio select n).ToList();
        }

        public IEnumerable<ViewEntidad> GetEntities()
        {
            return _context.ViewEntidad;
        }

        public IEnumerable<ViewBienservicio> GetHoldingServiceTypes()
        {
            return _context.ViewBienservicio;
        }

        public IEnumerable<ViewTipodebien> GetHoldingType()
        {
            return _context.ViewTipodebien;
        }

        public IEnumerable<ViewHomologacionLineaCredito> GetHomologatedCreditLines(IEnumerable<string> creditLines, bool isLegalPerson)
        {
            return _context.ViewHomologacionLineaCredito.Where(
                   a => creditLines.Contains(a.IdLineaCreditoLince) && a.Juridica == isLegalPerson);
        }

        public IEnumerable<ViewTipoidentificacion> GetIdentificationTypes()
        {
            return _context.ViewTipoidentificacion;
        }

        public IEnumerable<ViewEstadocivil> GetMaritalStatuses()
        {
            return _context.ViewEstadocivil;
        }

        public IEnumerable<ViewNacionalidad> GetNationalities()
        {
            return _context.ViewNacionalidad;
        }

        public IEnumerable<ViewOcupacion> GetOccupations()
        {
            return _context.ViewOcupacion;
        }

        public IEnumerable<ViewTiporelacionperson> GetPersonalRelationTypes()
        {
            return _context.ViewTiporelacionperson;
        }

        public IEnumerable<ViewPortafolio> GetPorfolios()
        {
            return _context.ViewPortafolio;
        }

        public IEnumerable<ViewProfesion> GetProfessions()
        {
            return _context.ViewProfesion;
        }

        public IEnumerable<ViewTiposociedad> GetSocietyTypes()
        {
            return _context.ViewTiposociedad;
        }

        public IEnumerable<ViewBienservicio> ViewBIENSERVICIO()
        {
            return _context.ViewBienservicio;
        }

        public IEnumerable<ViewTipodebien> ViewTIPODEBIEN()
        {
            return _context.ViewTipodebien;
        }
    }
}
