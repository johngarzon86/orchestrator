﻿using Orchestrator.Data.Interfaces;
using Orchestrator.Data.Models;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.ServiceModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Orchestrator.Data.Implementations
{
    public class LinceRepository : ILinceRepository
    {
        private CanalesvirtualesContext _context;
        private IRepository<IntentosGeneracionTin> _repositoryGeneracion;
        private IRepository<IntentosTin> _repositoryIntentos;

        public LinceRepository(CanalesvirtualesContext context, IRepository<IntentosGeneracionTin> repositoryGeneracion, IRepository<IntentosTin> repositoryIntentos)
        {
            _context = context;
            _repositoryGeneracion = repositoryGeneracion;
            _repositoryIntentos = repositoryIntentos;
        }
        public ProductoCanalesHomologacionLince GetApprovedProduct(CreditLine creditLine)
        {
            return _context.ProductoCanalesHomologacionLince.Where(x => x.LineaCredito.Trim() == creditLine.CreditLineCode
                                && x.TransaccionInterna.Trim() == creditLine.InternalTransactionCode
                                && x.CategoriaProducto.Trim() == creditLine.Product
                                && x.Portafolio == creditLine.PortfolioCode
                                ).First();
        }
        public ProductoCanalesHomologacionLince GetApprovedProduct(int approvedProductId)
        {
            return _context.ProductoCanalesHomologacionLince.Where(x => x.Id == approvedProductId).FirstOrDefault();
        }
        public ProductoCanales GetApprovedProductById(int approvedProductId)
        {
            if (!_context.ProductoCanalesHomologacionLince.Any(x => x.Id == approvedProductId))
            {
                return null;
            }
            var approvedProduct = _context.ProductoCanalesHomologacionLince.Where(x => x.Id == approvedProductId).SingleOrDefault();
            var productId = approvedProduct.IdProducto;
            return _context.ProductoCanales.Where(x => x.IdProducto == productId).SingleOrDefault();
        }
        public ViewTipoidentificacion GetDocumetTypeByCode(string docType)
        {
            var result = _context.ViewTipoidentificacion.Where(x => x.CodigoTipoIdentificacion.Equals(docType)).FirstOrDefault();
            return result;
        }
        public IEnumerable<ViewTipoidentificacion> GetDocumetTypes()
        {
            return _context.ViewTipoidentificacion;
        }
        public IEnumerable<ViewAreageografica> GetCities()
        {
            return _context.ViewAreageografica;
        }
        public IEnumerable<ViewPais> GetCountries()
        {
            return _context.ViewPais;
        }
        public IEnumerable<ViewProfesion> GetProfessions()
        {
            return _context.ViewProfesion;
        }
        public HomologacionPortafolio GetApprovedPortfolio(string approvedPortfolio)
        {
            return _context.HomologacionPortafolio.Where(x => x.Cmbccomak == approvedPortfolio).FirstOrDefault();
        }
        public ViewTipoidentificacion GetDocumetTypesByType(string documentType)
        {
            if (documentType == null) { throw new ArgumentNullException(documentType); }
            return _context.ViewTipoidentificacion.Where(x => x.CodigoTipoIdentificacion.Equals(documentType)).FirstOrDefault();
        }

        //public IEnumerable<DropDownListItem> GetApprovedProductLince(CreditLine creditLine, bool? isEducative = null)
        //{
        //    if (isEducative == null)
        //    {
        //        return (from hl in _context.ProductoCanalesHomologacionLince
        //                join pc in _context.ProductoCanales on
        //                   hl.IdProducto equals pc.IdProducto
        //                where hl.LineaCredito == creditLine.CreditLineCode
        //                   && hl.TransaccionInterna == creditLine.InternalTransactionCode
        //                   && hl.CategoriaProducto == creditLine.Product
        //                   && hl.Portafolio == pc.PortafolioXDefecto
        //                   && pc.Activo == true

        //                select new DropDownListItem($"{hl.Id}", pc.NombreProducto, ((double)pc.Cupo).ToString("F0", CultureInfo.InvariantCulture))).ToList();

        //    }

        //    return (from hl in _context.ProductoCanalesHomologacionLince
        //            join pc in _context.ProductoCanales on
        //               hl.IdProducto equals pc.IdProducto
        //            where hl.LineaCredito == creditLine.CreditLineCode
        //               && hl.TransaccionInterna == creditLine.InternalTransactionCode
        //               && hl.CategoriaProducto == creditLine.Product
        //               && hl.Portafolio == creditLine.PortfolioCode
        //               && pc.Activo == true
        //               && hl.EsEducativo == isEducative

        //            select new DropDownListItem($"{hl.Id}", pc.NombreProducto, ((double)pc.Cupo).ToString("F0", CultureInfo.InvariantCulture))).ToList();
        //}
        public IEnumerable<ApprovedProduct> GetApprovedProductLince(IEnumerable<CreditLine> creditlines, bool isEducative)
        {

            IEnumerable<string> internalTransactionCodes = creditlines.Select(c => c.InternalTransactionCode).Distinct();
            IEnumerable<string> products = creditlines.Select(c => c.Product).Distinct();
            IEnumerable<string> portfolios = creditlines.Select(c => c.PortfolioCode).Distinct();
            IEnumerable<string> creditLineCodes = creditlines.Select(c => c.CreditLineCode).Distinct();

            if (portfolios != null && portfolios.Count() == 1 && string.IsNullOrEmpty(portfolios.ToList()[0]))
            {
                return (from hl in _context.ProductoCanalesHomologacionLince
                        join pc in _context.ProductoCanales on
                           hl.IdProducto equals pc.IdProducto
                        where creditLineCodes.Contains(hl.LineaCredito)
                           && internalTransactionCodes.Contains(hl.TransaccionInterna)
                           && products.Contains(hl.CategoriaProducto)
                           && pc.Activo == true
                           && hl.EsEducativo == isEducative
                        select new ApprovedProduct
                        {
                            Id = hl.Id,
                            ProductId = hl.IdProducto,
                            ProductName = pc.NombreProducto,
                            Quota = pc.Cupo,
                            Portfolio = hl.Portafolio,
                            DefaultPortfolio = pc.PortafolioXdefecto,
                            DefaultPortfolioPj = pc.PortafolioXdefectoPj,
                        }).ToList();
            }

            return (from hl in _context.ProductoCanalesHomologacionLince
                    join pc in _context.ProductoCanales on
                       hl.IdProducto equals pc.IdProducto
                    where creditLineCodes.Contains(hl.LineaCredito)
                       && internalTransactionCodes.Contains(hl.TransaccionInterna)
                       && products.Contains(hl.CategoriaProducto)
                       && portfolios.Contains(hl.Portafolio)
                       && pc.Activo == true
                       && hl.EsEducativo == isEducative
                    select new ApprovedProduct
                    {
                        Id = hl.Id,
                        ProductId = hl.IdProducto,
                        ProductName = pc.NombreProducto,
                        Quota = pc.Cupo,
                        Portfolio = hl.Portafolio,
                        DefaultPortfolio = pc.PortafolioXdefecto,
                        DefaultPortfolioPj = pc.PortafolioXdefectoPj
                    }).ToList();
        }

        public List<ViewBienservicio> GetHoldingService(string CommerceCategory)
        {
            var service = from bs in _context.ViewBienservicio
                          where bs.IdCategoria == CommerceCategory
                          select bs;

            return service.Distinct().ToList();
        }

        public ViewAreageografica GetCityByCode(string cityCode)
        {
            var result = _context.ViewAreageografica.Where(x => x.CodigoAreaGeografica == cityCode).FirstOrDefault();
            return result;
        }
        #region TIN
        public List<IntentosGeneracionTin> GetTinGenerationAttemps(Dictionary<string, object> parameters)
        {
            var result = _context.IntentosGeneracionTin.
                            Where(x => x.Nit == decimal.Parse((string)parameters["menNit"]) &&
                                x.CedulaGirador == decimal.Parse((string)parameters["docNumber"]) &&
                                x.Fecha == ((TinData)parameters["TinData"]).Date).OrderByDescending(x => x.FechaRegistro).ToList();
            return result;
        }
        public int MaxTinGenerationAttemps()
        {
            if (!_context.ParametrosGenerales.Any(x => x.NombreParametro == "INTENTOS_GENERACION_TIN"))
                return 3;
            var parametro = (from parametros in _context.ParametrosGenerales
                             where parametros.NombreParametro == "INTENTOS_GENERACION_TIN"
                             select parametros.TextoParametro).FirstOrDefault();
            _ = Int32.TryParse(parametro, out int result);
            return result;
        }
        public int GenerationUnlockedTime()
        {
            if (!_context.ParametrosGenerales.Any(x => x.NombreParametro == "TIEMPO_DESBLOQUEO_TIN"))
                return 3;
            var parametro = (from parametros in _context.ParametrosGenerales
                             where parametros.NombreParametro == "TIEMPO_DESBLOQUEO_TIN"
                             select parametros.TextoParametro).FirstOrDefault();
            _ = Int32.TryParse(parametro, out int result);
            return result;
        }
        public async Task SaveTinGenerationLog(Dictionary<string, object> parameters)
        {
            if (parameters == null) { throw new ArgumentNullException(nameof(parameters), ""); }
            var obj = new IntentosGeneracionTin
            {
                Nit = decimal.Parse((string)parameters["menNit"]),
                CedulaGirador = decimal.Parse((string)parameters["docNumber"]),
                Fecha = ((TinData)parameters["TinData"]).Date,
                Hora = ((TinData)parameters["TinData"]).Hour,
                FechaRegistro = DateTime.Now,
                UsuarioRegistro = (string)parameters["userLogged"]
            };
            await Task.Run(() =>
            {
                _repositoryGeneracion.Add(obj);
            }).ConfigureAwait(false);
        }
        public async Task SaveTinAttempsLog(Dictionary<string, object> parameters)
        {
            if (parameters == null) { throw new ArgumentNullException(nameof(parameters), ""); }


            var attemp = _context.IntentosTin.Where(x => x.CedulaGirador == decimal.Parse((string)parameters["docNumber"]) &&
                                                        x.Fecha == ((TinData)parameters["TinData"]).Date &&
                                                        x.Hora == ((TinData)parameters["TinData"]).Hour).FirstOrDefault();
            if (attemp != null)
            {
                attemp.NumeroIntentos++;
                attemp.UltimaActualizacion = DateTime.Now;
                await Task.Run(() =>
                {
                    _repositoryIntentos.Update(attemp);
                }).ConfigureAwait(false);
            }
            else
            {
                var obj = new IntentosTin
                {
                    CedulaGirador = decimal.Parse((string)parameters["docNumber"]),
                    Fecha = ((TinData)parameters["TinData"]).Date,
                    Hora = ((TinData)parameters["TinData"]).Hour,
                    NumeroIntentos = 1,
                    UltimaActualizacion = DateTime.Now,
                    UsuarioActualizacion = (string)parameters["userLogged"]
                };
                await Task.Run(() =>
                {
                    _repositoryIntentos.Add(obj);
                }).ConfigureAwait(false);
            }
        }
        public int GetTinAttemps(Dictionary<string, object> parameters)
        {
            return (from intentos in _context.IntentosTin
                    where intentos.CedulaGirador == decimal.Parse((string)parameters["docNumber"]) &&
                        intentos.Fecha == ((TinData)parameters["TinData"]).Date &&
                        intentos.Hora == ((TinData)parameters["TinData"]).Hour
                    select intentos.NumeroIntentos).FirstOrDefault();
        }
        public int MaxTinAttemps()
        {

            if (!_context.ParametrosGenerales.Any(x => x.NombreParametro == "INTENTOS_TIN"))
                return 3;
            var parametro = (from parametros in _context.ParametrosGenerales
                             where parametros.NombreParametro == "INTENTOS_TIN"
                             select parametros.TextoParametro).FirstOrDefault();
            _ = Int32.TryParse(parametro, out int result);
            return result;
        }
        #endregion

        public IEnumerable<ViewTiporelacionperson> GetTypeRelationPerson()
        {
            return _context.ViewTiporelacionperson;
        }

        public IEnumerable<ViewOcupacion> GetOccupation()
        {
            return _context.ViewOcupacion;
        }

        public IEnumerable<ViewNiveldeestudio> GetLevelStudy()
        {
            return _context.ViewNiveldeestudio;
        }
        public IEnumerable<ViewInstitucioneducativa> EducationalInstitution()
        {
            return _context.ViewInstitucioneducativa.Where(x => x.CodigoPais == "170");
        }
    }
}
