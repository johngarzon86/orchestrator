﻿using Orchestrator.Data.Models;
using Orchestrator.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchestrator.Data.Repositories.CanalesVirtuales
{
    public class ActionsRepository : IActionsRepository
    {
        private readonly CanalesvirtualesContext _canalesvirtualesContext;
        public ActionsRepository(CanalesvirtualesContext context)
        {
            _canalesvirtualesContext = context;
        }
        public IEnumerable<Acciones> GetActions()
        {
            return _canalesvirtualesContext.Acciones.Where(x=>x.Activo == true);
        }
    }
}
