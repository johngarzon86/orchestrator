﻿using Orchestrator.Data.Models;
using Orchestrator.Domain.Models.Lince;
using Orchestrator.Domain.Models.ServiceModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Orchestrator.Data.Interfaces
{
    public interface ILinceRepository
    {
        /// <summary>
        /// Obtiene el producto homologado con Lince
        /// </summary>
        /// <returns></returns>
        ProductoCanalesHomologacionLince GetApprovedProduct(CreditLine creditLine);
        /// <summary>
        /// Obtiene lista de productos (DropDownListItem) homologados entre canales y lince, para un combo
        /// </summary>
        /// <returns></returns>
        ProductoCanalesHomologacionLince GetApprovedProduct(int approvedProductId);
        //IEnumerable<DropDownListItem> GetApprovedProductLince(CreditLine creditLine, bool? isEducative = null);
        IEnumerable<ApprovedProduct> GetApprovedProductLince(IEnumerable<CreditLine> creditlines, bool isEducative);
        ProductoCanales GetApprovedProductById(int approvedProductId);
        ViewTipoidentificacion GetDocumetTypeByCode(string docType);
        /// <summary>
        /// Obtiene los tipos documentales que hay en Lince
        /// </summary>
        /// <returns></returns>
        IEnumerable<ViewTipoidentificacion> GetDocumetTypes();
        /// <summary>
        /// Obtiene las ciudades que hay en Lince
        /// </summary>
        /// <returns></returns>
        IEnumerable<ViewAreageografica> GetCities();
        IEnumerable<ViewPais> GetCountries();
        ViewAreageografica GetCityByCode(string cityCode);
        IEnumerable<ViewProfesion> GetProfessions();
        HomologacionPortafolio GetApprovedPortfolio(string approvedPortfolio);
        ViewTipoidentificacion GetDocumetTypesByType(string documentType);
        List<IntentosGeneracionTin> GetTinGenerationAttemps(Dictionary<string, object> parameters);
        int MaxTinGenerationAttemps();
        int GenerationUnlockedTime();
        Task SaveTinGenerationLog(Dictionary<string, object> tim);
        Task SaveTinAttempsLog(Dictionary<string, object> tim);
        int GetTinAttemps(Dictionary<string, object> parameters);
        int MaxTinAttemps();

        /// <summary>
        /// Obtiene los Bien servicios de Lince
        /// </summary>
        /// <returns></returns>
        List<ViewBienservicio> GetHoldingService(string CommerceCategory);

        IEnumerable<ViewTiporelacionperson> GetTypeRelationPerson();

        IEnumerable<ViewOcupacion> GetOccupation();

        IEnumerable<ViewNiveldeestudio> GetLevelStudy();

        IEnumerable<ViewInstitucioneducativa> EducationalInstitution();
    }
}
