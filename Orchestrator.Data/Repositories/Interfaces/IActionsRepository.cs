﻿using Orchestrator.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchestrator.Data.Repositories.Interfaces
{
    public interface IActionsRepository
    {
        IEnumerable<Acciones> GetActions();
    }
}
