﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Orchestrator.Data.Interfaces
{
    public interface IRepository<T>
    {
        void Add(T entidad);
        void Delete(T entidad);
        int Update(T entidad);
        int Count(Expression<Func<T, bool>> where);

        //T GetById(int id);
        //IEnumerable<T> GetBy(ParametersQuery<T> parametersQuery);
    }
} 