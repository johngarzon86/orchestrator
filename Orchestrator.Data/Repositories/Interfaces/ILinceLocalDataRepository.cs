﻿
using Orchestrator.Data.Models;
using System.Collections.Generic;

namespace Orchestrator.Data.Interfaces
{
    public interface ILinceLocalDataRepository
    {
        IEnumerable<ViewBienservicio> GetHoldingServiceTypes();
        IEnumerable<ViewTipodebien> GetHoldingType();
        IEnumerable<ViewTipoidentificacion> GetIdentificationTypes();
        IEnumerable<ViewAreageografica> GetDemographicAreas();
        IEnumerable<ViewPais> GetCountries();
        IEnumerable<ViewActividadeconomica> GetEconomicActivities();
        IEnumerable<ViewNacionalidad> GetNationalities();
        IEnumerable<ViewNiveldeestudio> GetEducationLevels();
        IEnumerable<ViewProfesion> GetProfessions();
        IEnumerable<ViewEstadocivil> GetMaritalStatuses();
        IEnumerable<ViewOcupacion> GetOccupations();
        IEnumerable<ViewTiposociedad> GetSocietyTypes();
        IEnumerable<ViewTipoempresa> GetCompanyTypes();
        IEnumerable<ViewInstitucioneducativa> GetEducationalInstitutions();
        IEnumerable<ViewTiporelacionperson> GetPersonalRelationTypes();
        IEnumerable<ViewComercio> GetCommerces();
        IEnumerable<ViewCampanA> GetCampaigns();
        IEnumerable<ViewCanal> GetChannels();
        IEnumerable<ViewPortafolio> GetPorfolios();
        IEnumerable<ViewAgencia> GetAgencies();
        IEnumerable<ViewEntidad> GetEntities();
        IEnumerable<ViewHomologacionLineaCredito> GetHomologatedCreditLines(IEnumerable<string> creditLines,bool isLegalPerson);
    }
}
