﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class LegalPerson
    {
        [JsonProperty("razonSocial")]
        public string BusinessName  { get; set; }
    }
}
