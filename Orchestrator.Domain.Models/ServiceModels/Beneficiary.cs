﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class Beneficiary
    {
        [JsonProperty("tipoIdentificacion")]
        public string IdentificationType { get; set; }

        [JsonProperty("numeroIdentificacion")]
        public string IdentificationNumber { get; set; }

        [JsonProperty("primerNombre")]
        public string FirstName { get; set; }

        [JsonProperty("segundoNombre")]
        public string SecondName { get; set; }

        [JsonProperty("primerApellido")]
        public string SurName { get; set; }

        [JsonProperty("segundoApellido")]
        public string SecondSureName { get; set; }

        [JsonProperty("genero")]
        public string Gender { get; set; }

        [JsonProperty("paisNacimiento")]
        public string NativeCountry { get; set; }

        [JsonProperty("fechaNacimiento")]
        public string BirthDate { get; set; }

        [JsonProperty("estadoCivil")]
        public string MaritalStatus { get; set; }

        [JsonProperty("relacionDeudor")]
        public string DebtorRelationship { get; set; }

        [JsonProperty("institucionEducativa")]
        public string EducationalInstitution { get; set; }

        [JsonProperty("tipoPrograma")]
        public string TypeProgram { get; set; }

        [JsonProperty("programaEducativo")]
        public string EducationalProgram { get; set; }

        [JsonProperty("semestre")]
        public int Semester { get; set; }

        [JsonProperty("identificadorEstudiante")]
        public string StudentID { get; set; }

        [JsonProperty("salario")]
        public int Salary { get; set; }

        public static implicit operator List<object>(Beneficiary v)
        {
            throw new NotImplementedException();
        }
    }
}
