﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class WebLinePreApprovedRequestViewModel
    {
        [JsonProperty("codigoEntidad")]
        public string EntityCode { get; set; }

        [JsonProperty("tipoIdentificacion")]
        public string IdentificationType { get; set; }
        
        [JsonProperty("identificacion")]
        public string Identification { get; set; }
        
        [JsonProperty("origenIncorporacion")]
        public string OriginIncorporation { get; set; }

        [JsonProperty("codigoComercio")]
        public string BusinessCode { get; set; }

        [JsonProperty("transaccionInterna")]
        public string InternalTransaction { get; set; }

        [JsonProperty("codigoProducto")]
        public string ProductCode  { get; set; }

        [JsonProperty("codigoLineaCredito")]
        public string CreditLineCode { get; set; }

        [JsonProperty("fechaTransaccion")]
        public string TransactionDate { get; set; }

        [JsonProperty("horaTransaccion")]
        public string TransactionTime { get; set; }

        [JsonProperty("valorAval")]
        public double AvalValue  { get; set; }

        [JsonProperty("valorCapitalConsumo")]
        public double ValueCapitalConsumption { get; set; }

        [JsonProperty("numeroCuotas")]
        public short NumberQuotas { get; set; }
  
        [JsonProperty("usuario")]
        public string User { get; set; }
      
        [JsonProperty("planPagos")]
        public List<PreApprovedTitleViewModel> PaymentPlan { get; set; }

        [JsonProperty("beneficiarios")]
        public List<PreApprovedBeneficiariesViewModel> Beneficiaries { get; set; }
        
        [JsonProperty("datosContacto")]
        public string ContactDetails { get; set; }

        [JsonProperty("indTasaEspecial")]
        public string IndSpecialRate { get; set; }

        [JsonProperty("tasaEspecial")]
        public double SpecialRate  { get; set; }

        [JsonProperty("tipoProcesamiento")]
        public string ProcessingType  { get; set; }

        [JsonProperty("claveDigitalWeb")]
        public string WebDigitalkey { get; set; }
    }
}
