﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class TinData
    {
        public string Tin { get; set; }
        public string Date { get; set; }
        public string Hour { get; set; }
    }
}
