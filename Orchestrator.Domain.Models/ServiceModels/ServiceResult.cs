﻿

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class ServiceResult
    {
        public string ContentResult { get; set; }
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
        public string JsonResult { get; set; }
        public string StatusCode { get; set; }
        public ServiceError Error { get; set; }
    }
}
