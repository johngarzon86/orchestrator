﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class BiometryValidationResponseViewModel
    {
        /// <summary>
        /// Clase donde se declara la repuesta del servicio
        /// Solcitud de validacion de Biometria
        /// </summary>
        /// 

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("data")]
        public BiometryValidationResponseData Data { get; set; }

        [JsonProperty("codeName")]
        public string CodeName { get; set; }
    }
}
