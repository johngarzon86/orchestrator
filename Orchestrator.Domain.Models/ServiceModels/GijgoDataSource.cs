﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class GijgoDataSource
    {
        public string records { get; set; }
        public int total { get; set; }
        public string ErrorMessage { get; set; }
    }
}
