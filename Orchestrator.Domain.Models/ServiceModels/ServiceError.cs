﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class ServiceError
    {
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDenialCode { get; set; }
        public string ErrorDeniedby { get; set; }
    }
}
