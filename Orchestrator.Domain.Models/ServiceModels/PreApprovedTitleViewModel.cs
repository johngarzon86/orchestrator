﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class PreApprovedTitleViewModel
    {
        [JsonProperty("identificacionTitulo")]
        public string IdentificationTitle { get; set; }

        [JsonProperty("secuenciaTitulo")]
        public short TitleSequence { get; set; }

        [JsonProperty("valorTitulo")]
        public double TitleValue { get; set; }

        [JsonProperty("coberturaFlotante")]
        public double FloatingCoverage { get; set; }

        [JsonProperty("fechaVencimientoSpecified")]
        public string SpecifiedExpirationDate { get; set; }

        [JsonProperty("fechaVencimiento")]
        public DateTime ExpirationDate { get; set; }

        [JsonProperty("medioDePago")]
        public string PaymentMethod { get; set; }
    }
}
