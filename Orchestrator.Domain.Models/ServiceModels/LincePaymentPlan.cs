﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class LincePaymentPlan
    {
        [JsonProperty("numeroCuota")]
        public int NumberQuotas { get; set; }

        [JsonProperty("identTitulo")]
        public string TitleIdentification { get; set; }

        [JsonProperty("entidadCuenta")]
        public string AccountEntity { get; set; }

        [JsonProperty("numeroCuenta")]
        public string AccountNumber { get; set; }

        [JsonProperty("fechaAperturaCuenta")]
        public string DateOpeningAccount { get; set; }

        [JsonProperty("fechaVencimiento")]
        public string ExpirationDate { get; set; }

        [JsonProperty("valorTitulo")]
        public decimal TitleValue { get; set; }

        [JsonProperty("valorCobertura")]
        public decimal CoverageValue { get; set; }

        [JsonProperty("coberturaFlotante")]
        public decimal FloatingCoverage { get; set; }
    }
}
