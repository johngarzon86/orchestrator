﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class DocumentData
    {
        public string CommerceCode { get; set; }
        public string AutomaticDiscountStatus { get; set; }
        public string ProcessDate { get; set; }
        public string DigitalKey { get; set; }
        public string AuthorizationId { get; set; }

    }
}
