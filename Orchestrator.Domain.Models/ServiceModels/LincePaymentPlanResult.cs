﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class LincePaymentPlanResult
    {
        [JsonProperty("codigoAut")]
        public string CodeAut { get; set; }
        [JsonProperty("numCuota")]
        public string Fees { get; set; }
        [JsonProperty("idTitulo")]
        public string TitleId { get; set; }
        [JsonProperty("valTitulo")]
        public decimal TitleValue { get; set; }
        [JsonProperty("fechaVenc")]
        public string ExpirationDate { get; set; }
        [JsonProperty("medioPago")]
        public string PaymentMethod { get; set; }
        [JsonProperty("estCumpl")]
        public string estCumpl { get; set; }
        [JsonProperty("estDesc")]
        public string estDesc { get; set; }
        [JsonProperty("Desembolso")]
        public string Disbursement { get; set; }
        [JsonProperty("indSiniestro")]
        public string Sinister { get; set; }
        [JsonProperty("valorOtrosCargos")]
        public double OtherChargesValue { get; set; }
    }
}