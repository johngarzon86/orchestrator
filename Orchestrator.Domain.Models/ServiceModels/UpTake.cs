﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class UpTake
    {
        [JsonProperty("transaccionInterna")]
        public string InternalTransaction { get; set; }

        [JsonProperty("marca")]
        public string Brand { get; set; }

        [JsonProperty("categoriaProducto")]
        public string ProductCategory { get; set; }

        [JsonProperty("lineaCredito")]
        public string CreditLine { get; set; }

        [JsonProperty("moneda")]
        public string Currency { get; set; }

        [JsonProperty("valorConsumo")]
        public decimal UptakeValue { get; set; }

        [JsonProperty("numeroCuotas")]
        public int NumberQuotas { get; set; }

        [JsonProperty("valorCapitalConsumo")]
        public decimal ValueCapitalUptake { get; set; }

        [JsonProperty("destinacionBien")]
        public int DestinationHolding { get; set; }

        [JsonProperty("bienServicio")]
        public string HoldingService { get; set; }

        [JsonProperty("indTasaEspecial")]
        public string IdEspecialRate { get; set; }

        [JsonProperty("tasaEspecial")]
        public decimal? EspecialRate { get; set; }
    }
}
