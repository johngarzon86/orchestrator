﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class BiometryValidationResponseData
    {
        [JsonProperty("procesoConvenioGuid")]
        public string GuidAgreementProcess { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
