﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class GetCommerceSpecialRateResponse
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }

        [JsonProperty("comercio")]
        public string Business { get; set; }

        [JsonProperty("codLineaCredito")]
        public string CodCreditLine { get; set; }

        [JsonProperty("estadoConsulta")]
        public string QueryStatus { get; set; }

        [JsonProperty("codCamp")]
        public string CampainCode { get; set; }

        [JsonProperty("tasaDescEsq")]
        public string RateSchema { get; set; }

        [JsonProperty("tasas")]
        public IEnumerable<SpecialRateList> RateList { get; set; }
    }
}
