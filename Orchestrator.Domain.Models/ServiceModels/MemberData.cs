﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class MemberData
    {
        public string MemberIdentificationType { get; set; }
        public string MemberIdentificationNumber { get; set; }
    }
}
