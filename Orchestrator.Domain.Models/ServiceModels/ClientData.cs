﻿namespace Orchestrator.Domain.Models.ServiceModels
{
    public class ClientData
    {
        public string MemberIdentificationNumber { get; set; }
        public string IdentificationType { get; set; }
        public string IdentificationNumber { get; set; }
        public string CommerceCode { get; set; }
        public string User { get; set; }
    }
}
