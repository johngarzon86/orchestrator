﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class BiometrySearchResponseServiceViewModel
    {
        [JsonProperty("servicio")]
        public string Service { get; set; }

        [JsonProperty("tipo")]
        public string Type { get; set; }

        [JsonProperty("subTipos")]
        public string SubType { get; set; }

        [JsonProperty("score")]
        public string Score { get; set; }

        [JsonProperty("fecha")]
        public string Date { get; set; }

        [JsonProperty("terminado")]
        public string Finalized { get; set; }       
    }
}
