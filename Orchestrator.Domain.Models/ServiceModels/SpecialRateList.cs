﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class SpecialRateList
    {
        [JsonProperty("tasaEsp")]
        public string Rate { get; set; }

        [JsonProperty("porTasaDesc")]
        public string RatePercentage { get; set; }
    }
}
