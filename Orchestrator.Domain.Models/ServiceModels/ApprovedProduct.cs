﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class ApprovedProduct
    {
        public int Id { get; set; }
        public short ProductId { get; set; }
        public string ProductName { get; set; }
        public double? Quota { get; set; }
        public string Portfolio { get; set; }
        public string DefaultPortfolio { get; set; }
        public string DefaultPortfolioPj { get; set; }
    }
}
