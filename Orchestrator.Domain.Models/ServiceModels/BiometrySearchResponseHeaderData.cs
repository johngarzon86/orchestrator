﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class BiometrySearchResponseHeaderData
    {
        [JsonProperty("guidConv")]
        public string GuidConv { get; set; }

        [JsonProperty("procesoConvenioGuid")]
        public string GuidAgreementProcess { get; set; }

        [JsonProperty("primerNombre")]
        public string FirstName { get; set; }

        [JsonProperty("segundoNombre")]
        public string SecondName { get; set; }

        [JsonProperty("primerApellido")]
        public string SurName { get; set; }

        [JsonProperty("segundoApellido")]
        public string SecondSurName { get; set; }

        [JsonProperty("tipoDoc")]
        public string DocumentType { get; set; }

        [JsonProperty("numDoc")]
        public string DocumentNumber { get; set; }

        [JsonProperty("email")]
        public string EMail { get; set; }

        [JsonProperty("celular")]
        public string PhoneNumber { get; set; }

        [JsonProperty("asesor")]
        public string Consultant { get; set; }

        [JsonProperty("sede")]
        public string Branch { get; set; }

        [JsonProperty("nombreSede")]
        public string BranchName { get; set; }

        [JsonProperty("codigoCliente")]
        public string CustomerCode { get; set; }

        [JsonProperty("estadoProceso")]
        public string ProcessStatus { get; set; }

        [JsonProperty("scoreProceso")]
        public string ProcessScore { get; set; }

        [JsonProperty("aprobado")]
        public string Approved { get; set; }

        [JsonProperty("cancelado")]
        public string Canceled { get; set; }

        [JsonProperty("motivoCancelacion")]
        public string ReasonCancelation { get; set; }

        [JsonProperty("finalizado")]
        public string Finalized { get; set; }

        [JsonProperty("fechaRegistro")]
        public string RecordDate { get; set; }

        [JsonProperty("fechaFinalizacion")]
        public string FinishDate { get; set; }

        [JsonProperty("servicios")]
        public List<BiometrySearchResponseServiceViewModel> Services { get; set; }
    }
}
