﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class BiometryValidationRequestViewModel
    {
        [JsonProperty("GuidConv")]
        public string GuidConv { get; set; }

        [JsonProperty("TipoValidacion")]
        public string ValidationType { get; set; }

        [JsonProperty("Asesor")] 
        public string Consultant { get; set; }

        [JsonProperty("Sede")]
        public string Branch { get; set; }

        [JsonProperty("CodigoCliente")]
        public string CustomerCode { get; set; }

        [JsonProperty("TipoDoc")]
        public string DocumentType { get; set; }

        [JsonProperty("NumDoc")]
        public string DocumentNumber { get; set; }

        [JsonProperty("Email")]
        public string EMail { get; set; }

        [JsonProperty("Celular")]    
        public long PhoneNumber { get; set; }

        [JsonProperty("Usuario")]
        public string User { get; set; }

        [JsonProperty("Clave")]
        public string PassWord { get; set; }     
    }
}
