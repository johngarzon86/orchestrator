﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class BiometrySearchResponseHeaderViewModel
    {
        [JsonProperty("code")]
        public string code { get; set; }

        [JsonProperty("data")]
        public BiometrySearchResponseHeaderData Data { get; set; }

        [JsonProperty("codeName")]
        public string codeName { get; set; }
    }
}
