﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class CommerceData
    {
        public string CommerceIdentificationType { get; set; }
        public string CommerceIdentificationNumber { get; set; }
        public string CommerceCode { get; set; }
        public string Portfolio { get; set; }
    }
}
