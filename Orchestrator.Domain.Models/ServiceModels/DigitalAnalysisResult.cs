﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class DigitalAnalysisResult
    {
        [JsonProperty("servicio")]
        public string Service { get; set; }

        [JsonProperty("tipoServicio")]
        public string ServiceType { get; set; }

        [JsonProperty("subtipo")]
        public string SubType { get; set; }

        [JsonProperty("score")]
        public int Score { get; set; }

        [JsonProperty("fechaHora")]
        public string DateTime { get; set; }

        [JsonProperty("indterminacion")]
        public string IndTermination { get; set; }
    }
}
