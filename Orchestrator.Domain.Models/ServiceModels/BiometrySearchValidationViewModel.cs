﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class BiometrySearchValidationViewModel
    {
        [JsonProperty("GuidConv")]
        public string GuidConv { get; set; }

        [JsonProperty("ProcesoConvenioGuid")]
        public string GuidAgreementProcess { get; set; }

        [JsonProperty("Usuario")]
        public string User { get; set; }

        [JsonProperty("Clave")]
        public string PassWord { get; set; }
    }
}
