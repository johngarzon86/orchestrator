﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.ServiceModels
{
    public class NaturalPerson
    {
        [JsonProperty("primerNombre")]
        public string FirstName { get; set; }
        
        [JsonProperty("segundoNombre")]
        public string SecondName { get; set; }

        [JsonProperty("primerApellido")]
        public string SurName { get; set; }

        [JsonProperty("segundoApellido")]
        public string SecondSureName { get; set; }

        [JsonProperty("genero")]
        public int Gender { get; set; }

        [JsonProperty("ocupacion")]
        public string Profession { get; set; }
    }
}
