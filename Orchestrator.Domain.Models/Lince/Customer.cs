﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class Customer
    {
        [JsonProperty("disponibleCliente")]
        public double AvailableBalance { get; set; }
    }
}
