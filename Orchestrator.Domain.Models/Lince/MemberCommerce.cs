﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class MemberCommerce
    {
        [JsonProperty("codigoEntidad")]
        public string EntityCode { get; set; }
        [JsonProperty("tipoIdAfiliado")]
        public string MemberTipeId { get; set; }
        [JsonProperty("numIdAfiliado")]
        public string MemberId { get; set; }
        [JsonProperty("codExternoComercio")]
        public string CommerceExternalCode { get; set; }
        [JsonProperty("estadoConsulta")]
        public string State { get; set; }
        [JsonProperty("codigoComercio")]
        public string CommerceCode { get; set; }
        [JsonProperty("nombreComercio")]
        public string CommerceName{ get; set; }
        [JsonProperty("categoriaComercio")]
        public string CommerceCategory { get; set; }
        [JsonProperty("serviciosComercio")]
        public List<CommerceService> CommerceServices { get; set; }
    }
}
