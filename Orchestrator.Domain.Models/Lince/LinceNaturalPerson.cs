﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class LinceNaturalPerson
    {
        [JsonProperty("primerNombre")]
        public string FirstName { get; set; }
        [JsonProperty("primerApellido")]
        public string FirstSurname { get; set; }
        [JsonProperty("fechaNacimiento")]
        public string BirthDate { get; set; }
        [JsonProperty("genero")]
        public string Gender { get; set; }
        [JsonProperty("codigoOcupacion")]
        public string OccupationCode { get; set; }
        [JsonProperty("paisNacimiento")]
        public string BirthCountry { get; set; }
        [JsonProperty("ciudadResidencia")]
        public string ResidenceCity { get; set; }
    }
}
