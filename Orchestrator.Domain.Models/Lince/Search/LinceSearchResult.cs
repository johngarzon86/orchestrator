﻿using Orchestrator.Domain.Models.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince.Search
{
    public class LinceSearchResult
    {
        public string entity { get; set; }
        public string tipoIdGirador { get; set; }
        public string abrvTipoId { get; set; }
        public string idGirador { get; set; }
        public string nombreGirador { get; set; }
        public string codigoAut { get; set; }
        public string codProducto { get; set; }
        public string descProducto { get; set; }
        public string codLineaCredito { get; set; }
        public string descLineaCredito { get; set; }
        public string codEstadoAut { get; set; }
        public string descEstadoAut { get; set; }
        public string fechaAval { get; set; }
        public string horaAval { get; set; }
        public decimal valorAval { get; set; }
        public string codComercio { get; set; }
        public string nombreComercio { get; set; }
        public string ciudadComercio { get; set; }
        public string codRespuesta { get; set; }
        public string descRespuesta { get; set; }
        public string indBien { get; set; }
        public string tipoProcesamiento { get; set; }
        public string estadoDescAutom { get; set; }
        public bool Nuevo { get; set; }
        public IEnumerable<LincePaymentPlanResult> planPagos { get; set; }
    }
}
