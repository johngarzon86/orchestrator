﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince.Search
{
    public class LinceExtendedSearchFilter
    {
        [JsonProperty("entidad")]
        public string entity { get; set; }      

        [JsonProperty("tipoIdAfil")]
        public string tipoIdAfil { get; set; }

        [JsonProperty("idAfil")]
        public string idAfil { get; set; }

        [JsonProperty("tipoIdGir")]
        public string tipoIdGir { get; set; }

        [JsonProperty("idGir")]
        public string idGir { get; set; }
        
        [JsonProperty("codAut")]
        public string codAut { get; set; }

        [JsonProperty("fechaInicio")]
        public string InitialDate { get; set; }

        [JsonProperty("fechaFin")]
        public string FinalDate { get; set; }

        [JsonProperty("listaComer")]
        public string ComerList { get; set; }

        [JsonProperty("numReg")]
        public int NumReg { get; set; }

        [JsonProperty("indTotalCons")]
        public string indTotalCons { get; set; }

        [JsonProperty("returnPlanPagos")]
        public string ReturnPlanPayment { get; set; }

        [JsonProperty("returnBeneficiarios")]
        public string ReturnBeneficiaries { get; set; }
    }
}
