﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Orchestrator.Domain.Models.Lince.Search
{
    public class LinceBasicSearchFilter
    {
        [Display(Name = "Tipo documento")]
        public string entity { get; set; }

        [Display(Name = "Tipo identificación comercio")]
        public string tipoIdAfil { get; set; }

        [Display(Name = "Documento comercio")]
        public string idAfil { get; set; }

        [Display(Name = "Tipo identificación cliente")]
        public string tipoIdGir { get; set; }
        
        [Display(Name = "N° identificación cliente")]
        public string idGir { get; set; }

        [Display(Name = "Fecha solicitud")]
        public string fechaAval { get; set; }
        
        [Display(Name = "Número solicitud")]
        public string codAut { get; set; }

        [Display(Name = "ReturnPlanPagos")]
        public string returnPlanPagos { get; set; }

        [Display(Name = "ReturnBeneficiarios")]
        public string returnBeneficiarios { get; set; }

        [Display(Name = "fechaAval")]
        public string DateAval { get; set; }

        public int NumeroRegistros { get; set; }
    }
}
