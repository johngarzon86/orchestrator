﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Orchestrator.Domain.Models.Lince.Search
{
    public class LinceReportRequest
    {
        [Required(ErrorMessage = "Ingrese un valor válido")]
        [Display(Name = "*Fecha Inicio")]
        public string StartDate { get; set; }
        [Required(ErrorMessage = "Ingrese un valor válido")]
        [Display(Name = "*Fecha Fin")]
        public string EndDate { get; set; }
        [Required(ErrorMessage = "Ingrese un valor válido")]
        [Display(Name = "*Email 1")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Ingrese un email válido")]
        [EmailAddress(ErrorMessage = "Ingrese un email válido")]
        public string Email1 { get; set; }
        [Display(Name = "Email 2")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Ingrese un email válido")]
        [EmailAddress(ErrorMessage = "Ingrese un email válido")]
        public string Email2 { get; set; }
        [Display(Name = "Email 3")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Ingrese un email válido")]
        [EmailAddress(ErrorMessage = "Ingrese un email válido")]
        public string Email3 { get; set; }
        public string MemberDocument { get; set; }
        public string MemberDocumentType { get; set; }
    }
}
