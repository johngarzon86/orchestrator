﻿using Orchestrator.Domain.Models.ServiceModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class InputConfirmationRequestFinal: InputConfirmationRequestBaseFinal
    {

        [JsonProperty("consumo")]
        public UpTake Uptake { get; set; }

        [JsonProperty("titulos")]
        public List<LincePaymentPlan> Tittles { get; set; }

        [JsonProperty("beneficiarios")]
        public List<Beneficiary> Beneficiary { get; set; }

    }
}
