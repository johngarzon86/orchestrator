﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class AutomaticDiscountOutputModel
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }
        [JsonProperty("claveDigitalWeb")]
        public string DigitalWebPassword { get; set; }
        [JsonProperty("idAutorizacion")]
        public string AuthorizationId { get; set; } 
        [JsonProperty("codRespuesta")]
        public string ResponseCode { get; set; }
        [JsonProperty("descInconsistencia")]
        public string ResponseDesc { get; set; }
    }
}
