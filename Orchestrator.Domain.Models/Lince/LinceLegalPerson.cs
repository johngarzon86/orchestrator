﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class LinceLegalPerson
    {
        [JsonProperty("razonSocial")]
        public string BusinessName { get; set; }
    }
}
