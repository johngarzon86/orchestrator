﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class VerifiedClient
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }
        [JsonProperty("tipoIdentificacion")]
        public string IdentificationType { get; set; }
        [JsonProperty("numeroIdentificacion")]
        public string IdentificationNumber { get; set; }
        [JsonProperty("indicadorCliente")]
        public int ClientIndicator { get; set; }
        [JsonProperty("estadoConsulta")]
        public int Status { get; set; }
        [JsonProperty("claveDigitalWeb")]
        public string DigitalKey { get; set; }
        [JsonProperty("nombreComercio")]
        public string CommerceName { get; set; }
        [JsonProperty("categComercio")]
        public string CommerceCategory { get; set; }
        [JsonProperty("telCelular")]
        public string PhoneNumber { get; set; }
        [JsonProperty("lineasCred")]
        public IEnumerable<CreditLine> CreditLines { get; set; }
    }
}
