﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class CreditLine
    {
        [JsonProperty("codigoTransInt")]
        public string InternalTransactionCode { get; set; }
        [JsonProperty("codLineaCredito")]
        public string CreditLineCode { get; set; }
        [JsonProperty("producto")]
        public string Product { get; set; }
        [JsonProperty("codPortafolio")]
        public string PortfolioCode { get; set; }
    }
}
