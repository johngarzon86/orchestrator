﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class DigitalTransaction
    {
        [JsonProperty("tipoIdentificacion")]
        public string IdentificationType { get; set; }
        [JsonProperty("numeroIdentificacion")]
        public string IdentificationNumber { get; set; }
        [JsonProperty("nombreGirador")]
        public string CustomerName { get; set; }
        [JsonProperty("claveDigitalWeb")]
        public string DigitalWebPassword { get; set; }
        [JsonProperty("idAutorizacion")]
        public string AuthorizationId { get; set; }
        [JsonProperty("estadoProceso")]
        public string ProcessState { get; set; }
        [JsonProperty("estadoDescAutom")]
        public string DiscountState { get; set; }
        [JsonProperty("codigoAut")]
        public string CodeAut { get; set; }
        [JsonProperty("numSolicitud")]
        public string ApplicationNum { get; set; }
        [JsonProperty("codProducto")]
        public string ProductCode { get; set; }
        [JsonProperty("codLineaCredito")]
        public string CreditLineCode { get; set; }
        [JsonProperty("descLineaCredito")]
        public string CreditLineDescription { get; set; }
        [JsonProperty("codEstadoAut")]
        public string codEstadoAut { get; set; }
        [JsonProperty("descEstadoAut")]
        public string descEstadoAut { get; set; }
        [JsonProperty("fechaAval")]
        public string AvalDate { get; set; }
        [JsonProperty("horaAval")]
        public string AvalHour { get; set; }
        [JsonProperty("valorAval")]
        public double AvalValue { get; set; }
        [JsonProperty("cuotas")]
        public string Fees { get; set; }
        [JsonProperty("codRespuesta")]
        public string ResponseCode { get; set; }
        [JsonProperty("descRespuesta")]
        public string DescriptionResponse { get; set; }
    }
}
