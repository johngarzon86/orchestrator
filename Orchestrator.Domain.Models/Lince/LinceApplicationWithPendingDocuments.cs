﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class LinceApplicationWithPendingDocuments
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }
        [JsonProperty("comercio")]
        public string Commerce { get; set; }
        [JsonProperty("nombreComercio")]
        public string CommerceName { get; set; }
        [JsonProperty("transaccionesDigitales")]
        public List<DigitalTransaction> DigitalTransactions { get; set; }
    }
}
