﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class AutomaticDigitalDiscountActivation
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }

        [JsonProperty("claveDigitalWeb")]
        public string DigitalWebPassword { get; set; }

        [JsonProperty("idAutorizacion")]
        public string IdAuthorization { get; set; }

        [JsonProperty("idDigPagare")]
        public string idDigPayment { get; set; }
    }
}
