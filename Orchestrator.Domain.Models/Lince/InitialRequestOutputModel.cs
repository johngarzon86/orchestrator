﻿using Orchestrator.Domain.Models.ServiceModels;
using Newtonsoft.Json;

namespace Orchestrator.Domain.Models.Lince
{
    public class InitialRequestOutputModel
    {
        [JsonProperty("numSolicitud")]
        public string ApplicationNumber { get; set; }

        [JsonProperty("causaNegacionSol")]
        public string GroundsforDenial { get; set; }

        [JsonProperty("claveDigitalWeb")]
        public string DigitalWebPassword { get; set; }

        [JsonProperty("MyProperty")]
        public string MyProperty { get; set; }
    }
}
