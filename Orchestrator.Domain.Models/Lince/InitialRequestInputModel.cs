﻿using Orchestrator.Domain.Models.ServiceModels;
using Newtonsoft.Json;

namespace Orchestrator.Domain.Models.Lince
{
    public class InitialRequestInputModel
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }
        [JsonProperty("claveDigitalWeb")]
        public string DigitalWebPassword { get; set; }
        [JsonProperty("tipoIdentificacion")]
        public string IdentificationType { get; set; }
        [JsonProperty("numeroIdentificacion")]
        public string IdentificationNumber { get; set; }
        [JsonProperty("comercio")]
        public string Commerce { get; set; }
        [JsonProperty("canal")]
        public string Channel { get; set; }
        [JsonProperty("portafolio")]
        public string Briefcase { get; set; }
        [JsonProperty("cupoSolicitado")]
        public decimal RequestedQuota { get; set; }
        [JsonProperty("medioPagoInicial")]
        public int InitialPaymentMethod { get; set; }
        [JsonProperty("ciudadUbicacion")]
        public string CityLocation { get; set; }
        [JsonProperty("causaNegacionSol")]
        public string DeniedReason { get; set; }
        [JsonProperty("tipoProcesamiento")]
        public int ProcessingType { get; set; }
        [JsonProperty("personaJuridica")]
        public LegalPerson LegalPerson { get; set; }
        [JsonProperty("personaNatural")]
        public NaturalPerson NaturalPerson { get; set; }
    }
}
