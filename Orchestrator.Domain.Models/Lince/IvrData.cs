﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class IvrData
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }
        [JsonProperty("tipoId")]
        public string IdentificationType { get; set; }
        [JsonProperty("numeroId")]
        public string IdentificationNumber { get; set; }
        [JsonProperty("indicadorSolic")]
        public string ClientIndicator { get; set; }
        [JsonProperty("personaNatural")]
        public LinceNaturalPerson NaturalPerson { get; set; }
        [JsonProperty("personaJuridica")]
        public LinceLegalPerson LegalPerson { get; set; }
        [JsonProperty("cliente")]
        public Customer Customer { get; set; }
    }
}
