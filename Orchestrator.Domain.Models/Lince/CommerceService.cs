﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class CommerceService
    {
        [JsonProperty("codigoTransInt")]
        public string TransactionCode { get; set; }
        [JsonProperty("descTransInt")]
        public string TransactionDesc { get; set; }
        [JsonProperty("codProducto")]
        public string ProductCode { get; set; }
        [JsonProperty("descProducto")]
        public string ProductDesc { get; set; }
        [JsonProperty("codLineaCredito")]
        public string CreditLineCode { get; set; }
        [JsonProperty("descLineaCredito")]
        public string CreditLineDesc { get; set; }
    }
}
