﻿using Orchestrator.Domain.Models.ServiceModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class RecordDenialRequestInputModel
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }

        [JsonProperty("numSolicitud")]
        public string RequestNumber { get; set; }

        [JsonProperty("causaNegacionSol")]
        public string ReasonDenialRequest { get; set; }

        [JsonProperty("tipoProcesamiento")]
        public string ProcessingType { get; set; }

        [JsonProperty("claveDigitalWeb")]
        public string DigitalWebPassword { get; set; }

        //[JsonProperty("direccionEmail")]
        //public string EmailAddress { get; set; }

        //[JsonProperty("analisis")]
        //public List<DigitalAnalysisResult> Analysis { get; set; }      
    }
}
