﻿using Orchestrator.Domain.Models.ServiceModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orchestrator.Domain.Models.Lince
{
    public class InputConfirmationRequestBaseFinal
    {
        [JsonProperty("entidad")]
        public string Entity { get; set; }

        [JsonProperty("numSolicitud")]
        public string ApplicationNumber { get; set; }

        [JsonProperty("claveDigitalWeb")]
        public string DigitalWebPassword { get; set; }

        [JsonProperty("tipoIdentificacion")]
        public string IdentificationType { get; set; }

        [JsonProperty("numeroIdentificacion")]
        public string IdentificationNumber { get; set; }

        [JsonProperty("comercio")]
        public string Commerce { get; set; }

        [JsonProperty("canal")]
        public string Channel { get; set; }

        [JsonProperty("portafolio")]
        public string Briefcase { get; set; }

        [JsonProperty("cupoSolicitado")]
        public decimal RequestedQuota { get; set; }

        [JsonProperty("medioPagoInicial")]
        public int InitialPaymentMethod { get; set; }

        [JsonProperty("ciudadUbicacion")]
        public string CityLocation { get; set; }

        [JsonProperty("tipoProcesamiento")]
        public int ProcessingType { get; set; }

        [JsonProperty("tipoTelefono")]
        public string PhoneType { get; set; }

        [JsonProperty("numeroTelefono")]
        public string PhoneNumber { get; set; }

        [JsonProperty("contactoAfiliado")]
        public string MemberContactName { get; set; }

        [JsonProperty("telefonoContactoAfil")]
        public string MemberContactPhone { get; set; }

        [JsonProperty("estadoConsultaRiesgo")]
        public int StatusQueryRisk { get; set; }

        [JsonProperty("cupoSugerido")]
        public decimal SuggestedQuota { get; set; }

        [JsonProperty("direccionEmail")]
        public string EmailAddress { get; set; }

        [JsonProperty("direccionUbicacion")]
        public string Address { get; set; }

        [JsonProperty("referenciaUno")]
        public string FirstReferenceName { get; set; }

        [JsonProperty("celularRefUno")]
        public string FirstReferencePhone { get; set; }

        [JsonProperty("referenciaDos")]
        public string SecondReferenceName { get; set; }

        [JsonProperty("celularRefDos")]
        public string SecondReferencePhone { get; set; }

        [JsonProperty("personaJuridica")]
        public LegalPerson LegalPerson { get; set; }

        [JsonProperty("personaNatural")]
        public NaturalPerson NaturalPerson { get; set; }

        [JsonProperty("analisis")]
        public List<DigitalAnalysisResult> Analysis { get; set; }
    }
}
