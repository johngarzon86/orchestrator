using System.Text.Json.Serialization;

namespace Orchestrator.Domain.Models.Authentication
{
    public class UserDb
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
    }
}