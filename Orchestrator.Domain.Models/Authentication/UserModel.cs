﻿
using System.ComponentModel.DataAnnotations;

namespace Orchestrator.Domain.Models.Authentication
{
    public class UserModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string EmailAddress { get; set; }
    }

}
