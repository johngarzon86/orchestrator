﻿using Microsoft.AspNetCore.Mvc;
using Orchestrator.Domain.Interfaces;
using Orchestrator.Domain.Models.Authentication;
using Orchestrator.Domain.Services;

namespace Orchestrator.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IActionsDomain _actionsDomain;

        public UsersController(IUserService userService, IActionsDomain actionsDomain)
        {
            _userService = userService;
            _actionsDomain = actionsDomain;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        //[Authorize]
        //[HttpGet]
        //public IActionResult GetAll()
        //{
        //    var users = _userService.GetAll();
        //    return Ok(users);
        //}


        [Authorize]
        [HttpGet]
        public JsonResult GetAllActions()
        {
            return new JsonResult(_actionsDomain.GetActions());
        }
    }
}
